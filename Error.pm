package Valkyr::Error;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use vars qw($VERSION $VERBOSE $DEBUG $STACKWIDTH @EXPORT_OK %EXPORT_TAGS);

use Valkyr::Util::Token qw(__PROGRAM__ __BADGE__ __FUNCTION__ __THREAD__);
use Valkyr::Error::Base qw(__stacktrace);
use Valkyr::Error::Exception qw(TypeError AssertionError);
use Valkyr::Error::Constants qw(DEFAULT_STACKWIDTH ROOTCLASS BASECLASS);
use Valkyr::Class::Constants qw(REFS SUBS EMPTY PKG THREAD_MAIN);


BEGIN {
    no strict REFS;
    no strict SUBS;
    *EXCEPTION_BASECLASS = sub () { ROOTCLASS().PKG.BASECLASS };
    *EXCEPTION_TAG  = sub () {  'ERROR' };
    *WARNING_TAG    = sub () {  'WARN'  };
    *DEFAULT_TAG    = sub () {  'INFO'  };
    *DEBUGGING_TAG  = sub () {  'DEBUG' };
}

BEGIN {
    $VERSION  = eval '0.0.3';
    @EXPORT_OK
	= qw(throw warn report debug assert __tag __stacktrace_header 
            __stacktrace_footer __fold_line __get_config_hash 
            __format_simpleline __format_stacktrace);
    %EXPORT_TAGS
	= ('all' => [@EXPORT_OK[0..4]],
	   'dev' => [@EXPORT_OK[5..11]]);
    
    $STACKWIDTH = DEFAULT_STACKWIDTH if ! defined($STACKWIDTH);
    $VERBOSE    = 0 if ! defined($VERBOSE);
    $DEBUG      = 0 if ! defined($DEBUG);
}


sub __format_localtime {
    my ($s,$m,$h,$d,$M,$Y) = localtime;
    return sprintf('%02d-%02d-%02d %02d:%02d:%02d,000',1900+$Y,1+$M,$d,$h,$m,$s);
}

sub __format_prefix {
    my $type = shift || DEFAULT_TAG;
    return sprintf('%s %-5s %s',__format_localtime(), $type, __BADGE__)
}

# ----------------------------------------------------------------------
# Function  : __format_simpleline()
# Purpose   : Produce simple Warning or Exception line
#             (without the stacktrace)
# Arguments : Config hashref and string message(s)
# Returns   : String
# Comments  : Private. Written to allow modular customization of 
#             warning reporting API 
# ----------------------------------------------------------------------

sub __format_simpleline {
    my ($tag,$error) = @_;

    my $type    = $tag || EMPTY;
    my $class   = $error->class()   // EMPTY;
    my $message = $error->message() // EMPTY;
    
    #$type  .= ': ' if length($type);
    #$class .= ': ' if length($class);

    return join(EMPTY, __format_prefix($type), ' ',$message, "\n");
}


# ----------------------------------------------------------------------
# Function  : __stacktrace_width()
# Purpose   : The user interfaces with global $STACKWIDTH, this method 
#             provides a fallback layer against non-sane user input 
#             values: non-numeric types, or numeric values less than the
#             width of the program name badge
# Arguments : None
# Returns   : Integer. 
# Comments  : Private. Written to allow modular customization of
#             exception stacktrace API 
# ----------------------------------------------------------------------

sub __stacktrace_width {
    if (defined($STACKWIDTH) && 
	$STACKWIDTH =~ /^\d+$/ && $STACKWIDTH >= 30+length(__BADGE__)) {
	return $STACKWIDTH;
    } else {
	return DEFAULT_STACKWIDTH;
    }
}

# ----------------------------------------------------------------------
# Function  : __stacktrace_header()
# Purpose   : Produce a pretty header line for the stacktrace
# Arguments : None
# Returns   : Stringified header line
# Comments  : Private. Written to allow modular customization of
#             exception stacktrace API 
# ----------------------------------------------------------------------

sub __stacktrace_header {
    my $type   = shift || EMPTY;
    my $prefix = __format_prefix($type);
    my $width  = __stacktrace_width() - length($prefix);
    
    return join(EMPTY, $prefix, '_' x ($width > 0 ? $width : 0));
}

# ----------------------------------------------------------------------
# Function  : __stacktrace_footer()
# Purpose   : Produce a pretty footer line for the stacktrace
# Arguments : None
# Returns   : Stringified footer line
# Comments  : Private. Written to allow modular customization of 
#             exception stacktrace API 
# ----------------------------------------------------------------------

sub __stacktrace_footer {
    return __stacktrace_header(@_);
    # return '_' x __stacktrace_width();
}

# ----------------------------------------------------------------------
# Function  : __fold_line()
# Purpose   : Wrap lines to the width of the stack with 
#             hanging indent
# Arguments : String
# Returns   : Line-folded string
# Comments  : Private. Written to allow modular customization of 
#             exception stacktrace API 
# ----------------------------------------------------------------------

sub __fold_line {
    @_ = split(/\s/, join(EMPTY, @_ ));

    my $sep = ' ';  # separator
    my @par = (EMPTY); # paragraph
    my $width = __stacktrace_width() - length(__BADGE__) - 30;
    while (defined( my $word = shift )) {
	if ((length($par[$#par])+length($word)) < $width) {
	    $par[$#par] .= $sep.$word;
	} else {
	    push @par,(' 'x4).$word;
	}
    }
    
    return @par;
}

# ----------------------------------------------------------------------
# Function  : __format_stacktrace()
# Purpose   : Produce pretty stacktrace string
# Arguments : Config hashref and string message(s)
# Returns   : String
# Comments  : Private. Written to allow modular customization of 
#             exception stacktrace API
# ----------------------------------------------------------------------

sub __format_stacktrace {
    my ($tag,$error) = @_;

    my $type    = $tag || DEFAULT_TAG;
    my $class   = $error->class()   // EMPTY;
    my $message = $error->message() // EMPTY;
    my $thread  = $error->thread()  || __THREAD__ || THREAD_MAIN;

    # $type  .= ': ' if length($type);
    $class .= ': ' if length($class);
    
    # avoid spurious newlines in stacktrace if no traceback
    return join(EMPTY,__stacktrace_header($type),"\n",
		(map { length() ? __format_prefix($type)." $_\n" : EMPTY }
		      __fold_line($class . "Thread $thread: ", $message), 
    		      $error->stacktrace() || __stacktrace(1)),
		      __stacktrace_footer($type), "\n" );
}

# ----------------------------------------------------------------------
# Function  : throw()
# Purpose   : Produce pretty exception stacktrace string to STDERR
#             and then exit by die()
# Arguments : Valkyr::Error::Exception object
# Returns   : None
# Comments  : Public. Produces simple exception line (such as in warn(); 
#             without stacktrace) if $Valkyr::Error::VERBOSE < 0
# ----------------------------------------------------------------------

sub throw {
    if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }

    my $error = ! UNIVERSAL::isa( $_[0], EXCEPTION_BASECLASS )
	? EXCEPTION_BASECLASS()->new(@_)
	: shift;
    
    if ($VERBOSE < 0) {
	CORE::die( __format_simpleline( EXCEPTION_TAG, $error ));
    } else { 
	CORE::die( __format_stacktrace( EXCEPTION_TAG, $error ));
    }    
}

# ----------------------------------------------------------------------
# Function  : warn()
# Purpose   : Produce simple warning string to STDERR
# Arguments : Valkyr::Error::Exception object or string
# Returns   : None
# Comments  : Public. Valkyr::Error::Exception type not reported 
#             without use of Valkyr::Error::Exception. Produces
#             stacktrace if $Valkyr::Error::VERBOSE > 0
# ----------------------------------------------------------------------

sub warn {
    if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }

    my $error = ! UNIVERSAL::isa( $_[0], EXCEPTION_BASECLASS )
	? EXCEPTION_BASECLASS()->new(@_)
	: shift;

    if (($DEBUG > 0) || ($VERBOSE > 0)) { 
        syswrite(STDERR, __format_stacktrace( WARNING_TAG, $error ));
    } else {
	syswrite(STDERR, __format_simpleline( WARNING_TAG, $error ));
    }
}

# ----------------------------------------------------------------------
# Function  : assert()
# Purpose   : Test a condition
# Arguments : Statement evaluating to a bool, and a string or 
#             Valkyr::Error::Exception-derived object
# Returns   : None
# Comments  : Public. Used for consistency with API. Produces
#             stacktrace if $Valkyr::Error::DEBUG > 0, and a 
#             simple error message otherwise.
# ----------------------------------------------------------------------

sub assert (&;@) {
    if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }

    my $func = shift;
    ref($func) eq 'CODE' || 
	throw TypeError(__FUNCTION__,'() arg 1 must be a coderef.');

    my $true = 0;
    {
	local $@ = undef;
	$true = eval { $func->() };
	$true = 0 if $@;
    }

    if ( ! $true ) {
	my $error = ! UNIVERSAL::isa( $_[0], EXCEPTION_BASECLASS )
            ? AssertionError(@_)
            : shift;
	if ($DEBUG > 0) {
	    CORE::die( __format_stacktrace( EXCEPTION_TAG, $error ));
	} else {
	    CORE::die( __format_simpleline( EMPTY, $error ));
	}
    }
}

# ----------------------------------------------------------------------
# Function  : report()
# Purpose   : Produce simple reporting string to STDERR
# Arguments : String
# Returns   : None
# Comments  : Public. Used for consistent reporting with API.
# ----------------------------------------------------------------------

sub report {
    if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }
    # my $report = ! UNIVERSAL::isa( $_[0], EXCEPTION_BASECLASS )
    # 	? EXCEPTION_BASECLASS()->new(@_)
    #     : shift;

    syswrite(STDERR, join(EMPTY,__format_prefix(),' ',@_,"\n"));
}

# ----------------------------------------------------------------------
# Function  : debug()
# Purpose   : Produce simple debugging string to STDERR
# Arguments : String
# Returns   : None
# Comments  : Public. Used for consistent debugging with API. Produces
#             debugging message if $Valkyr::Error::DEBUG > 0, and a 
#             stacktrace if also $Valkyr::Error::VERBOSE > 0
# ----------------------------------------------------------------------

sub debug {
    if ($DEBUG > 0) {
	if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }
	my $error = ! UNIVERSAL::isa( $_[0], EXCEPTION_BASECLASS )
	    ? EXCEPTION_BASECLASS()->new(@_)
	    : shift;

	if ($VERBOSE > 0) {
	    syswrite(STDERR, __format_stacktrace( DEBUGGING_TAG, $error ));
	} else {
	    syswrite(STDERR, __format_simpleline( DEBUGGING_TAG, $error ));
	}
    }
}


1;

__END__


=head1 NAME

Valkyr::Error - Reporting for Valkyr::Error::Exception objects

=head1 SYNOPSIS

Import the subroutines:

B<NOTE>: C<Valkyr::Error> does not export any of its subroutines
by default, the user must import them explicitly:

    use Valkyr::Error qw(throw warn report debug);
    use Valkyr::Error::Exception qw(:all);

It is then possible to:

    throw Exception("Exception message.");

    warn ValueError("Input not of type 'Foo'.");

    report "Some output";
          -or-
    report AssertionError("This is probably a bug.");

    debug "Some code";
          -or-
    debug StandardError("Unnecessary, but complete.");

    assert {$a < 1} "Not what we expected.";

=head1 DESCRIPTION

It is the author's philosophy that there be a division of labor 
between modules that perform error reporting and modules that 
perform functions other than error reporting; In otherwords, this 
module is not written to be inherited from, but rather should be 
C<use>d in one's own code.

This represents a more customizable (see below) error-throwing 
mechanism than BioPerl's C<Bio::Root::Root>, C<Bio::Root::RootI>, 
or C<Bio::Root::Exception> modules in that there are still multiple
degrees of severity recognized by C<warn()>, C<throw()>, and 
C<debug()>, and in addition the stack-width may be changed by user 
preference.

All public methods append a newline to the end of the input message
string by default.

=head1 GLOBAL VARIABLES

=head2 $Valkyr::Error::VERSION 

  Purpose : Byte string representing this module's version
  Type    : Byte string
  
=head2 $Valkyr::Error::VERBOSE

  Purpose : Set/get verbosity level
  Type    : int/float

=head2 $Valkyr::Error::DEBUG

  Purpose : Set/get debugging level
  Type    : int/float

=head2 $Valkyr::Error::STACKWIDTH

  Purpose : Set/get stacktrace width (in characters)
  Type    : int

=head1 SUBROUTINES

=head2 throw()

  Usage    : throw BaseException( "message string" );
  Purpose  : Print stacktrace to STDERR and exit program.
  Args     : Valkyr::Error::Exception::BaseException-derived 
             object.
  Comments : Prints stacktrace by default; Print simple warning line
             when $Valkyr::Error::VERBOSE < 0.

=head2 warn()

  Usage    : warn BaseException( "message string" );
  Purpose  : Print warning message to STDERR.
  Args     : Valkyr::Error::Exception::BaseException-derived 
             object.
  Comments : Prints simple warning line by default; Prints a stack
             -trace when $Valkyr::Error::VERBOSE > 0 or when 
             $Valkyr::Error::DEBUG > 0.

=head2 report()

  Usage    : report "message string";
                     -or-
             report BaseException( "message string" );
  Purpose  : Emit a message to STDERR.
  Args     : Valkyr::Error::Exception::BaseException-derived 
             object or string.
  Comments : Prints only a simple reporting line. Also reports error 
             type if passed a subclass object.

=head2 debug()

  Usage    : debug "message string";
                     -or-
             debug BaseException( "message string" );
  Purpose  : Emit debugging info to STDERR.
  Args     : Valkyr::Error::Exception::BaseException-derived
             object or string.
  Comments : Does nothing by default; Prints simple debugging line
             when $Valkyr::Error::DEBUG > 0; Prints a stack
             -trace with debugging info when 
             $Valkyr::Error::VERBOSE > 0. Also reports error 
             type if passed a subclass object.

=head2 assert()

  Usage    : assert BLOCK "message string";
  Purpose  : Test that a statement is true, or throw a stackstrace.
  Args     : Test BLOCK and a Valkyr::Error::Exception::BaseException
             -derived object or message string.
  Comments : Cannot use enclosing braces, unfortunately. Chose
             evaluating the assert statement as a block, instead of
             in-place as Carp::assert does, because we cannot 
             guarentee perl will not produce on of its ugly native 
             warning messages if the test is not syntactically 
             correct, e.g. 'foo' == 1


=head1 INCOMPATIBILITIES

This module requires Perl 5.7.3 or higher.

=head1 AUTHORS

Jessen Bredeson <jessenbredeson@berkeley.edu>

=head1 BUGS AND LIMITATIONS

Please report bugs to the Author(s).

No mechanism yet to catch and re-throw errors, but it's in the works.
This module also has not yet been extensively tested.

=cut

# TOOOs
# retrows a pretty stack within a pretty stack if an eval {}'d die()
# is re-die()d. fix this.
