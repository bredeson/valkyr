package Valkyr::Class::Base;

use strict;
use warnings;

use Scalar::Util 'reftype';
use Valkyr::Util::Token qw(__BADGE__ __FUNCTION__);

use vars qw($VERSION);

BEGIN { $VERSION = eval '0.0.1' }


sub __initialize {
    return;
}

sub new {
    my ($pkg,@args) = @_;

    my $class = ref($pkg) || $pkg;
    my $self  = bless({},$class);

    $self->__initialize(@args);

    return $self;
}

# -----------------------------------------------------------------------------
# Introspection:
# -----------------------------------------------------------------------------

sub NAME { return ref($_[0])||$_[0] }

sub TYPE { return reftype(shift) }

sub ROLE { return UNIVERSAL::DOES(shift,shift||'') ? 1 : 0 }

sub ISA  { return UNIVERSAL::isa(shift,shift||'')  ? 1 : 0 }
*isa = \&ISA;

sub CAN  { return UNIVERSAL::can(shift,shift||'')  ? 1 : 0 }
*can = \&CAN;

sub BASE_NAME { (shift->NAME) =~ /^(.*)::/; return $1 }

sub DERIVED_NAME { (shift->NAME) =~ /(\w+)$/; return $1 }

sub VERSION { 
    my $ERROR;
    my $version;
    {
	local $SIG{__WARN__} = sub {($ERROR .= shift)=~s/^(.*)\bat\b.*/$1\n/m};
	local $SIG{__DIE__}  = sub {($ERROR .= shift)=~s/^(.*)\bat\b.*/$1\n/m};
	$version = eval "UNIVERSAL::VERSION(\@_)";
    }
    if ($ERROR) { die(__BADGE__," $ERROR") };

    return $version;
}

# -----------------------------------------------------------------------------
# Generic Object Methods
# -----------------------------------------------------------------------------

sub string { return shift->NAME() }


1;
