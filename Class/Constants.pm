package Valkyr::Class::Constants;

use Exporter 'import';
use vars qw(@EXPORT_OK %EXPORT_TAGS);

BEGIN {
    @EXPORT_OK   = qw(THREAD_MAIN RECURSION EMPTY ONCE REFS SUBS PKG ISA);
    %EXPORT_TAGS = ('all' => \@EXPORT_OK);
    *THREAD_MAIN = sub () {    'main'   };
    *RECURSION   = sub () { 'recursion' };
    *EMPTY       = sub () {      ''     };
    *ONCE        = sub () {    'once'   };
    *REFS        = sub () {    'refs'   };
    *SUBS        = sub () {    'subs'   };
    *PKG         = sub () {     '::'    };
    *ISA         = sub () { PKG().'ISA' };
}

1;

__END__
