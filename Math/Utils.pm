package Valkyr::Math::Utils;

use 5.010;
use strict;
use warnings;

use Exporter 'import';
use Valkyr::Error 'throw';
use Valkyr::Error::Exception qw(TypeError ValueError);

use Valkyr::Util::Token qw(__FUNCTION__);

use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert qw(is_uint is_sint is_ufloat is_sfloat is_string);

use vars qw(@EXPORT_OK);

BEGIN { 
    @EXPORT_OK 
	= qw(e E pi inv umod heavyside dirac ceil floor round 
             sign log argmax max argmin min sum argrange range);
}

# KNOWN BUG: max(@array[ <indicies> ]) syntax doesnt work as expected!

sub __gtNum { is_sfloat($_[0]) >  $_[1] }
sub __gtStr { is_string($_[0]) gt $_[1] }
sub __ltNum { is_sfloat($_[0]) <  $_[1] }
sub __ltStr { is_string($_[0]) lt $_[1] }
sub __eqNum { is_sfloat($_[0]) == $_[1] }
sub __eqStr { is_string($_[0]) eq $_[1] }

sub e { return exp( is_sfloat(shift // 1)) }
sub E { return 10** is_sfloat(shift // 1)  }
sub pi () { return 3.14159265358979323846  }


sub heavyside {
    # half-max convention, use ceil(heavyside($N)) for discrete
    return is_sfloat($_[0]) == 0 ? 0.5 : int($_[0] > 0);
}


sub inv {
    return is_sfloat($_[0]) == 0 ? 9**9**9 : (1/$_[0]);
}


sub umod {
    return is_sfloat($_[0]) == 0 ? 0.5 : $_[0] < 0 ? -1 : 1;
}


sub dirac {
    return is_sfloat(shift) == 0 ? 9**9**9 : 0;
}


sub log {
    my $base = is_ufloat(shift) || throw ValueError('ufloat base expected.');
    my $val  = is_ufloat(shift) || throw ValueError('Non-zero uint expected.');
    return CORE::log($val)/CORE::log($base);
}


sub sign {
    return is_sfloat($_[0]) == 0 ? 0 : $_[0] < 0 ? -1 : 1;
}


sub ceil {
    my $val  = is_sfloat(shift);
    return $val-int($val) <= 0 ? int($val) : int($val)+1 ;
}


sub floor {
    return int(is_sfloat(shift));
}


sub round {
    # use pos. values of 's' to round s sig-figs to the right of the
    # decimal and neg. values of 's' to round s sig-figs to the left
    # of the decimal.
    my $f = is_sfloat(shift);
    my $d = is_sint(shift||0); 
    return int($f * 10**$d + 0.5) / 10**$d;
}


sub argmax (+;+@) { 
    my $block = shift;
    my $input = shift; 
    my $isStr = shift;

    if (!Valkyr::Data::Type::Test::is_coderef($block)) { 
	$isStr = $input;
	$input = $block;
	if (Valkyr::Data::Type::Test::is_hashref($input)) {
	    $block = sub {$input->{ $_ }};
	} else {
	    $block = sub {$input->[ $_ ]};
	}
    } 
    
    my ($gt,$eq) = $isStr
        ? ( \&__gtStr, \&__eqStr )
	: ( \&__gtNum, \&__eqNum );

    my @idx; 
    if (Valkyr::Data::Type::Test::is_arrayref($input)) {
	my $max = $block->($_ = 0);
	for($_  = 0; $_ <= $#{$input}; ++$_) { 
	    my $val  = $block->($_); 
	    if ($gt->($val,$max)) {
		@idx = $_;
		$max = $val;
	    } elsif ($eq->($val,$max)) {
		push(@idx,$_);
	    }
	} 
    } elsif (Valkyr::Data::Type::Test::is_hashref($input)) {
	my @key = keys(%$input);
	my $max = $block->($_ = $key[0]); 
	for $_ (@key) {
	    my $val  = $block->($_);
	    if ($gt->($val,$max)) {
		@idx = $_;
		$max = $val;
	    } elsif ($eq->($val,$max)) {
		push(@idx,$_);
	    }
	}
    } else { 
 	throw TypeError('Must pass a hash(-ref)/array(-ref) to ',__FUNCTION__,'().');
    }
    return wantarray ? @idx : $idx[0];
}


sub max (;+@) { 
    my $data;
    if (Valkyr::Data::Type::Test::is_arrayref($_[0])) { 
	$data = $_[0] ;
    } elsif (Valkyr::Data::Type::Test::is_hashref($_[0])) {
	throw TypeError('Must pass a list to ',__FUNCTION__,'().');
    } else { 
	$data = \@_;
    } 
    my $max   = $data->[0];
    for(my $i = 0; $i <= $#{$data}; ++$i) { 
	if (__gtNum($data->[$i],$max)) { 
	    $max = $data->[$i];
	}
    } 
    return $max;
}


sub argmin (+;+@) { 
    my $block = shift;
    my $input = shift; 
    my $isStr = shift;

    if (!Valkyr::Data::Type::Test::is_coderef($block)) { 
	$isStr = $input;
	$input = $block;
	if (Valkyr::Data::Type::Test::is_hashref($input)) {
	    $block = sub {$input->{ $_ }};
	} else {
	    $block = sub {$input->[ $_ ]};
	}
    } 
    
    my ($lt,$eq) = $isStr
        ? ( \&__ltStr, \&__eqStr )
	: ( \&__ltNum, \&__eqNum );

    my @idx; 
    if (Valkyr::Data::Type::Test::is_arrayref($input)) {
	my $min = $block->($_ = 0); 
	for($_  = 0; $_ <= $#{$input}; ++$_) { 
	    my $val  = $block->($_); 
	    if ($lt->($val,$min)) {
		@idx = $_;
		$min = $val;
	    } elsif ($eq->($val,$min)) {
		push(@idx,$_);
	    }
	} 
    } elsif (Valkyr::Data::Type::Test::is_hashref($input)) {
	my @key = keys(%$input);
	my $min = $block->($_ = $key[0]); 
	for $_ (@key) {
	    my $val  = $block->($_);
	    if ($lt->($val,$min)) {
		@idx = $_;
		$min = $val;
	    } elsif ($eq->($val,$min)) {
		push(@idx,$_);
	    }
	}
    } else { 
 	throw TypeError('Must pass a hash(-ref)/array(-ref) to ',__FUNCTION__,'().');
    }
    return wantarray ? @idx : $idx[0];
}


sub min (;+@) { 
    my $data;
    if (Valkyr::Data::Type::Test::is_arrayref($_[0])) { 
	$data = $_[0] ;
    } elsif (Valkyr::Data::Type::Test::is_hashref($_[0])) {
	throw TypeError('Must pass a list to ',__FUNCTION__,'().');
    } else { 
	$data = \@_;
    } 
    my $min   = $data->[0];
    for(my $i = 0; $i <= $#{$data}; ++$i) { 
	if (__ltNum($data->[$i],$min)) { 
	    $min = $data->[$i];
	}
    }
    return $min;
}


sub argrange (+;@) {
    my $input = shift;
    my $isStr = is_uint(shift||0);
    
    my ($gt,$lt,$v_max,$v_min,$a_min,$a_max) = $isStr
        ? ( \&__gtStr, \&__ltStr, chr(0), chr(177), '', '')
	: ( \&__gtNum, \&__ltNum, -9**9**9, 9**9**9, 0, 0);

    if (Valkyr::Data::Type::Test::is_arrayref($input)) {
	for (my $i = 0; $i < @$input; ++$i) {
	    if ($lt->($input->[$i],$v_min)) {
		$v_min = $input->[$i];
		$a_min = $i;
	    }
	    if ($gt->($input->[$i],$v_max)) {
		$v_max = $input->[$i];
		$a_max = $i;
	    } 
	}
    } elsif (Valkyr::Data::Type::Test::is_hashref($input)) {
	for my $k (keys(%$input)) {
	    if ($lt->($input->{$k},$v_min)) {
		$v_min = $input->{$k};
		$a_min = $k;
	    }
	    if ($gt->($input->{$k},$v_max)) {
		$v_max = $input->{$k};
		$a_max = $k
	    }
	}
    } else {
	throw TypeError('Must pass a hash-ref/array-ref to ',__FUNCTION__,'().');
    }
    return($a_min,$a_max);
}


sub range {
    @_ || throw TypeError('List required at ',__FUNCTION__,'().');
    return @_[ argrange(@_) ];
}


sub sum {
    my $sum = 0;
    map { $sum += is_sfloat($_||0) } @_;
    return $sum;
}


1;
