package Valkyr::Math::Statistics::Tests;

use strict;
use warnings;

use base 'Exporter';

use Valkyr::Error qw(throw warn);
use Valkyr::Error::Exception qw(ValueError TypeError);
use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert qw(is_uint is_ufloat is_arrayref);

use constant 'PI' => 3.14159265358979;
use vars qw(@EXPORT_OK);

BEGIN { @EXPORT_OK = qw(init_table chiSquaredTest) }

# sub G {
#     # G = sum(N(0,1);-inf() -> z) = 0.5 * (1 + erf(Z))
#     # where Z = (x - X) / sd(x)
#     my $z = is_ufloat(shift);

#     local $@ = undef;
    
#     my $erf = eval { # try:
# 	erf($z);
#     }; if ($@) { # except:
# 	throw($@);
#     }

#     return 0.5 * (1 + $erf)
# }

# sub __chiSquaredTestAlpha_even {
#     my $x = is_ufloat(shift);
#     my $n = is_uint(shift);
    
#     my $m = ($n/2) - 1;
#     my ($u,$s) = (1,0);
#     for (my $i = 0; $i <= $m; ++$i) {
# 	$s += $u;
# 	$u *= ($x / (2 * ($i+1)));
#     }
#     my $alpha  = is_ufloat(
# 	$s * exp( -$x / 2 )
# 	);

#     if ($alpha > 1) { 
# 	$alpha = 1 
#     }

#     return $alpha;
# }

# sub __chiSquaredTestAlpha_odd {
#     my $x = is_ufloat(shift);
#     my $n = is_uint(shift);

#     my $m = ($n - 1) / 2;
#     my ($u,$s) = (1,0);
#     for (my $i = 0; $i < $m; ++$i) {
# 	$s += $u;
# 	$u *= ($x / (2 * ($i+1) + 1));
#     }
#     my $alpha  = is_ufloat( 
# 	2 * (1 - G( sqrt($x) )) 
# 	    + sqrt( (2 * $x) / PI ) * exp( -$x / 2 ) * $s
# 	);
    
#     if ($alpha > 1) { 
# 	$alpha = 1 
#     }

#     return $alpha;
# }

# sub __chiSquaredTestPvalue {
#     my $x = is_ufloat(shift); # chi-squared value
#     my $n = is_uint(shift);   # degrees of freedom

#     if ($n % 2) { # n is odd
# 	return __chiSquaredTestAlpha_odd($x,$n);
#     } else {
# 	return __chiSquaredTestAlpha_even($x,$n);
#     }
# }


# ----------------------------DISCLAIMER------------------------------
# See Math::Distributions (by Michael Kospach) for the two functions
# immediately below. I did NOT write them and do NOT claim any credit
# for them.
# --------------------------------------------------------------------

# Math::Distributions::_subuprob :
sub _subuprob {
    my ($x) = @_;
    my $p = 0; # if ($absx > 100)
    my $absx = abs($x);
    
    if ($absx < 1.9) {
	$p = (1 +
	      $absx * (.049867347
		       + $absx * (.0211410061
				  + $absx * (.0032776263
					     + $absx * (.0000380036
							+ $absx * (.0000488906
								   + $absx * .000005383)))))) ** -16/2;
    } elsif ($absx <= 100) {
	for (my $i = 18; $i >= 1; $i--) {
	    $p = $i / ($absx + $p);
	}
	$p = exp(-.5 * $absx * $absx) 
	    / sqrt(2 * PI) / ($absx + $p);
    }
    
    $p = 1 - $p if ($x<0);
    return $p;
}

# Math::Distributions::_subchisqrprob :
sub __chiSquaredTestPercentage {
    my $n = is_uint(shift);   # <- JVB add
    my $x = is_ufloat(shift); # <- JVB add
    my $p;

    if ($x <= 0) {
	$p = 1;
    } elsif ($n > 100) {
	$p = _subuprob((($x / $n) ** (1/3)
			- (1 - 2/9/$n)) / sqrt(2/9/$n));
    } elsif ($x > 400) {
	$p = 0;
    } else {   
	my ($a, $i, $i1);
	if (($n % 2) != 0) {
	    $p = 2 * _subuprob(sqrt($x));
	    $a = sqrt(2/PI) * exp(-$x/2) / sqrt($x);
	    $i1 = 1;
	} else {
	    $p = $a = exp(-$x/2);
	    $i1 = 2;
	}
	
	for ($i = $i1; $i <= ($n-2); $i += 2) {
	    $a *= $x / $i;
	    $p += $a;
	}
    }
    return $p;
}

# --------------------------------------------------------------------

sub __chiSquaredTestStatistic {
    my $observed = is_arrayref(shift);
    my $expected = is_arrayref(shift);
    my $N_tot = is_uint(shift);
    my $n = is_uint(shift);

    my $yates = 0;
    if ($n == 1 && $N_tot < 10) { 
	warn("Expected counts ($N_tot) less than ",
	     "10, applying Yates' correction.");
	$yates = 0.5;
    }

    my $x = 0;
    for (my $i = 0; $i < scalar(@$observed); ++$i) {
	for (my $j  = 0; $j < scalar(@{$observed->[$i]}); ++$j) {
	    my $obs = $observed->[$i]->[$j];
	    my $exp = $expected->[$i]->[$j];
	    if (!defined($exp) || $exp == 0) {
		throw ValueError('Illegal division by zero.');
	    }
	    $x += (( abs($obs - $exp) - $yates)** 2) / $exp;
	}
    }
    return $x;
}

sub __tableDimensions {
    my $table = is_arrayref(shift);

    my $dimensions = 0;

    my $N_tot = 0;
    my $cols  = 0;
    my $rows  = scalar(@$table);
    for (my $i = 0; $i < $rows; ++$i) {
	if (ref($table->[$i])) {
	    my ($c,$r,$s) = __tableDimensions($table->[$i]);
	    if (($cols && $c != $cols) || $r > 1) { $dimensions = 3 }
	    $dimensions |= 2;
	    $N_tot += $s;
	    $cols   = $c;
	} else {
	    is_uint($table->[$i]);
	    $dimensions |= 1;
	    $N_tot += $table->[$i];
	    $cols   = 1;
	}
	if ($dimensions == 3) {
	    throw TypeError('Mix-dimensioned table given.');
	}
    }
    return($rows,$cols,$N_tot);
}

sub init_table {
    my $rows = is_uint(shift);
    my $cols = is_uint(shift);
    my $val  = @_ ? is_uint(shift) : 0;

    if ($rows < 1 || $cols < 1) {
	throw TypeError("Invalid table dimensions ($rows,$cols).");
    }
    my $table = [];
    for (my $i = 0; $i < $rows; ++$i) {
	for (my $j = 0; $j < $cols; ++$j) {
	    $table->[$i]->[$j] = $val;
	}
    }
    return $table;
}

sub __chiSquaredExpectedTable {
    my $observed = is_arrayref(shift);
    my $rows  = is_uint(shift);
    my $cols  = is_uint(shift);

    my $esum = 0;
    my $osum = 0;
    my $expected = init_table($rows,$cols);
    my @hori_margin = ();
    my @vert_margin = ();
    for (my $i = 0; $i < $rows; ++$i) {
	for(my $j = 0; $j < $cols; ++$j) {
	    my $val = $observed->[$i]->[$j];
	    $vert_margin[$i] += $val;
	    $hori_margin[$j] += $val;
	    $osum += $val;
	}
    }
    for (my $i = 0; $i < $rows; ++$i) {
    	for(my $j = 0; $j < $cols; ++$j) {
	    my $val = int( (($vert_margin[$i] * $hori_margin[$j]) / $osum) + 0.5 );
    	    $expected->[$i]->[$j] = $val;
	    $esum += $val;
	}
    }
    return($expected,$esum);
}  

sub chiSquaredTest {
    my $observed  = is_arrayref(shift);
    my $expected;
    
    if (Valkyr::Data::Type::Test::is_uint($observed->[0])) {
	$observed = [ $observed ];
    } elsif (! Valkyr::Data::Type::Test::is_arrayref($observed->[0])) {
	throw TypeError("Invalid input data type ('observed'): $observed->[0].");
    }

    my ($orows,$ocols,$ototal) = __tableDimensions($observed);
    my ($erows,$ecols,$etotal) = ($orows,$ocols,$ototal);

    my $n = 0;
    if ( @_ ) {
	$expected = is_arrayref(shift);
	if (Valkyr::Data::Type::Test::is_uint($expected->[0])) {
	    $expected = [ $expected ];
	} elsif (! Valkyr::Data::Type::Test::is_arrayref($observed->[0])) {
	    throw TypeError("Invalid input data type ('expected'): $expected->[0].");
	}

	($erows,$ecols,$etotal) = __tableDimensions($expected);
	if ($erows != $orows || $ecols != $ocols) {
	    throw TypeError("'observed' and 'expected' tables different dimensions.");
	} elsif ($orows < $ocols) {
	    ($orows, $ocols) = ($ocols, $orows);
	}
	$n = ($orows - 1) * $ocols;

    } else {
	($expected) = __chiSquaredExpectedTable($observed,$orows,$ocols);
	$etotal = $ototal;
	$n = ($orows - 1) * ($ocols - 1);
    }

    if ($etotal < 5) {
	warn("Expected counts ($etotal) less than 5, result is dubious.");
    }
    if ($n < 1) {
	throw ValueError("Too few degrees of freedom: $n.");
    }

    my $x = __chiSquaredTestStatistic($observed,$expected,$etotal,$n);
    my $P = __chiSquaredTestPercentage($n,$x);

    return($P,$x,$n);
}

1;
