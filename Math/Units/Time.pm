package Valkyr::Math::Units::Time;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use Valkyr::Error 'throw';
use Valkyr::Error::Exception 'ValueError';
use Valkyr::Data::Type::Assert qw(is_string is_ufloat);

use vars qw(@EXPORT_OK);

@EXPORT_OK = qw(HMStoS StoHMS);

my $HMS_STRING_PAT = qr/(\d+):(\d+):(\d+)/;


sub HMStoS {
    my $S  = is_string(shift);
    
    if($S !~ $HMS_STRING_PAT) {
	throw ValueError("Not a time string in HH:MM:SS format: $S");
    }
    return(3600*$1 + 60*$2 + $3);
}


sub StoHMS {
    my $S = is_ufloat(shift||0);
    my $m = $S % 3600;
    my $s = $m %   60;
    return sprintf('%02u:%02u:%02u',int($S/3600),int($m/60),int($s))
}


1;
