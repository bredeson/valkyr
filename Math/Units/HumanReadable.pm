package Valkyr::Math::Units::HumanReadable;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use Valkyr::Error 'throw';
use Valkyr::Error::Exception qw(TypeError ValueError);
use Valkyr::Math::Utils qw(log min round);
use Valkyr::Data::Type::Assert qw(is_uint is_sfloat is_string);

use vars qw(@EXPORT_OK);

@EXPORT_OK = qw(abbrev expand);

# Assuming the Short (American) Scale...
# Powers of 1000:
my $SUPPORTED_UNITS = qr/^(\d+\.?\d*)([a-zA-Z]?)$/i;
my %EXPAND_SHORT_UNITS;
my %ABBREV_SHORT_UNITS = (
    0 =>  '', 1 => 'k', 
    2 => 'M', 3 => 'G',
    4 => 'T', 5 => 'P',
    6 => 'E', 7 => 'Z',
    8 => 'Y'
);
$EXPAND_SHORT_UNITS{'D'} = 1/3;
$EXPAND_SHORT_UNITS{'H'} = 2/3;
for my $u (keys(%ABBREV_SHORT_UNITS)) {
    $EXPAND_SHORT_UNITS{ uc($ABBREV_SHORT_UNITS{$u}) } = $u;
}


sub __as_float {
    my $v = shift;
    my $s = shift;
    
    return $s < 0 ? $v : sprintf('%0.'.$s.'f',$v);
}


sub abbrev {
    my $f = is_sfloat(shift);
    my $s = is_uint(shift||0);
    my $u = min(8,int(log(1000,abs($f+0)||1)));
    return __as_float(round($f / 1000**$u,$s),$s).$ABBREV_SHORT_UNITS{$u}
}


sub expand {
    my $s = is_string(shift);
    
    if($s =~ s/$SUPPORTED_UNITS//) {
	if (defined($EXPAND_SHORT_UNITS{uc($2)})) {
	    return $1 * 1000**$EXPAND_SHORT_UNITS{uc($2)};
	} else {
	    throw ValueError("Unit out of range: $2.");
	}
    }
    throw TypeError("Not a valid human-readable numerical value: $s.");
}



1;
