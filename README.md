README
======

Purpose
-------

Perl modules for programming. Provides functions for raising errors and type checking, and reading some common bioinformatics formats.


Installation
------------
```
# Clone the repository
git clone git@bitbucket.org:bredeson/valkyr.git Valkyr

# Add repository path to PERL5LIB
echo 'export PERL5LIB=/path/to/Valkyr:$PERL5LIB' >>~/.bash_profile
```

Bugs and Issues
---------------
Any comments or bug reports should be directed to the author.

Author
------
Jessen V. Bredeson <jessenbredeson@berkeley.edu>