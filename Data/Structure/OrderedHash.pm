package Valkyr::OrderedHash;

use 5.010;
use strict;
use warnings;

use lib '.';

use Valkyr::Error 'throw';
use Valkyr::Data::Type::Test qw(is_string is_subclass is_object);

sub __validate_field {
    my ($self,$key) = @_;
    is_string($key) || 
	throw('Field key must be a string, received: \'',
	      $key//'undef','\'.');
    $key =~ /^\w+$/ || 
	throw('Field key is empty or contains invalid ',
	      'characters, received: \'',$key,'\'.');
    return 1;
}

sub __validate_value {
    my ($self,$val) = @_;
    # optionally get the key because value range may be 
    # dependent on key, ex: my ($self,$val,$key) = @_;
    return 1;
}

sub __index { return shift->{'_index'} }

sub __field { return shift->{'_field'} }

sub clear {
    my $self = shift;
    $self->{'_index'} = [];
    $self->{'_field'} = {};
    return 1;
}

sub __initialize_new {
    my ($self,@args) = @_;

    # clear() initializes '_index' and '_field' internal hash keys
    $self->clear();
    throw('Unmatched args received.') if @args % 2; 
    # odd-numbered args; we expect a hash
    while(my $key = shift @args) {
	my $val = shift @args;
	$key =~ s/^-*//;
        # set_field(), though implementing clear() to clear/
	# re-initialize the object, may not always do this in 
	# its derived classes
	$self->set_field($key,$val); 
    }
    return 1;
}

sub new {
    my ($ref,@args) = @_;
    
    my $class = ref($ref) || $ref || __PACKAGE__;
    my $self  = bless({},$class);
    
    $self->__initialize_new(@args);
 
    return $self;
}

sub get_field {
    my ($self,$key) = @_;
    $self->__validate_field($key);
    return $self->__field()->{'_'.$key}; # enforce case-sensitivity
}

sub get_field_keys {
    return @{shift->__index()};
}

sub set_field {
    my ($self,$key,$val) = @_;
    $self->__validate_field($key);
    $self->__validate_value($val,$key);
    $self->delete_field($key) if $self->exists_field($key);
    $self->__field()->{'_'.$key} = $val;
    push @{$self->__index()},$key;
    
    return 1;
}

sub update_field {
    my ($self,$key,$val) = @_;
    $self->__validate_field($key);
    $self->__validate_value($val,$key);
    $self->__field()->{'_'.$key} = $val;

    return 1;
}

sub exists_field {
    my ($self,$key) = @_;
    $self->__validate_field($key);
    return exists($self->__field()->{'_'.$key});
}

sub delete_field {
    my ($self,$key) = @_;
    $self->exists_field($key) || return;

    my $i = 0;
    my $keys = $self->__index();
    while ($keys->[$i] ne $key) { $i++ }
    splice(@$keys,$i,1) && delete($self->__field()->{'_'.$key});

    return 1;
}    

# sub __clone {
#     my $self = shift;

#     # no cicular references, please.
#     no warnings 'recursion';
#     return $self->__clone_recursive($self);
# }

# sub __clone_recursive {
#     my ($self,$val) = @_;
#     my $clone;
#     if (is_string($val)) {
# 	return $val;
#     } elsif (ref($val) eq 'HASH' || is_subclass($val,'HASH')) {
# 	for my $key (keys %$val) {
# 	    $clone->{$key} = $self->__clone_recursive($val->{$key});
# 	}
#     } elsif (ref($val) eq 'ARRAY' || is_subclass($val,'ARRAY')) {
# 	$clone = [];
# 	for(my $i = 0; $i < @$val; ++$i) {
# 	    $clone->[$i] = $self->__clone_recursive($val->[$i]);
# 	}
#     } elsif (ref($val) eq 'SCALAR' || is_subclass($val,'SCALAR')) {
# 	$$clone = $self->__clone_recursive($$val);
#     } else {
# 	throw('Unsupported type for clone: ',ref($val)||$val//'undef','.');
#     }
#     if (is_object($val)) {
# 	return bless($clone,ref($val));
#     } else {
# 	return $clone;
#     }
# }


1;

##fileformat=VCFv4.X required

##INFO=<ID=ID,Number=number,Type=type,Description="description"> (strict)
# ID: a word, no spaces or commas, case-sensitive
# Types: Integer, Float, Flag, Character, and String
# Number=0 (flag)
# Number=# (array of numbers #-long)
# Number=A (one for each alt allele)
# Number=G (one for each genotype)
# Number=. (array of indeterminate size)

##FILTER=<ID=ID,Description="description"> (strict)

##FORMAT=<ID=ID,Number=number,Type=type,Description="description"> (strict)

##ALT=<ID=type,Description="description"> (strict) ID must be:
# ID=DEL
# ID=INS
# ID=DUP
# ID=INV
# ID=CNV

##assembly=url

##contig=<ID=ctg1,URL=ftp://somewhere.org/assembly.fa,...>

##SAMPLE=<ID=S_ID,Genomes=G1_ID;G2_ID; ...;GK_ID,Mixture=N1;N2; ...;NK,Description=S1;S2; ...; SK>

##PEDIGREE=<Name_0=G0-ID,Name_1=G1-ID,...,Name_N=GN-ID>

##pedigreeDB=<url>
