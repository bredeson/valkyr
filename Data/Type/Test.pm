package Valkyr::Data::Type::Test;

require 5.008;

use strict;
use warnings;
use Exporter 'import';

use lib '.';
use vars qw($VERSION $VERBOSE $DEBUG $STRINGPAT $BOOLPAT $UINTPAT $SINTPAT
            $UFLOATPAT $SFLOATPAT $HEXIPAT $OCTALPAT %VALID_TEST $CODE_ERROR
            @EXPORT_OK %EXPORT_TAGS);

use Valkyr::Error qw(throw debug);
use Valkyr::Error::Exception qw(ValueError TypeError);

BEGIN {
    $DEBUG   = 0;
    $VERBOSE = 0;
    $VERSION = eval '0.3.0';
    @EXPORT_OK = qw(is_bool is_uint is_sint is_ufloat is_sfloat is_string
                    is_char is_hexi is_octal is_regex is_code is_arrayref 
                    is_stringref is_hashref is_coderef is_object is_subclass 
                    is_instance is_globref is_file);
    %EXPORT_TAGS = ('all'    => \@EXPORT_OK,
		    'file'   => [qw(is_globref is_file)],
		    'number' => [qw(is_uint is_sint is_ufloat is_sfloat 
                                    is_hexi is_octal)],
		    'string' => [qw(is_string is_char is_regex is_scalarref 
                                    is_stringref)],
		    'list'   => [qw(is_arrayref is_hashref)],
		    'ref'    => [qw(is_arrayref is_scalarref is_stringref
                                    is_hashref is_coderef is_globref)],
		    'object' => [qw(is_object is_subclass is_instance)]);
    %VALID_TEST  = map { $_ => 1 } 
                    qw(f d l p S D c t T B e z s r w x o R W X O u g k);

    # pre-compile the regexs to be more efficient:
    $STRINGPAT = qr/^[[:print:][:cntrl:]]*$/;
    $BOOLPAT   = qr/^(?:1|T|TRUE|0|F|FALSE)$/i;
    $UINTPAT   = qr/^\d+$/;
    $SINTPAT   = qr/^[+-]?\d+$/;
    $UFLOATPAT = qr/^\d*\.?\d+(?:[eE][-+]?\d+\.?\d*)?$/;
    $SFLOATPAT = qr/^[-+]?\d*\.?\d+(?:[eE][-+]?\d+\.?\d*)?$/;
    $HEXIPAT   = qr/^0x[\da-fA-F]+$/;
    $OCTALPAT  = qr/^[0-7]+$/;
}

# I choose to allow undefined values to be passed to these subs
# so that they may be tested rather than dying a horrible death

# Caveat: Perl does not differentiate 1.00 from 1; they are both
# stored as integers in memory when coerced into the numeric type.

# the POSIX module makes a statement that implies \d eq [:digit:],
# so use \d throughout for brevity

sub __stash_code_error { 
    $CODE_ERROR = shift;
    $CODE_ERROR =~ s/^\s+|\s+$//g;
    return $CODE_ERROR;
}

sub is_string { # a printable one, even if empty
    my $val = shift;
    
    # in order to be a string, we must also be defined
    return(ref(\$val) eq 'SCALAR' && defined($val) && $val =~ $STRINGPAT || 0);
}

sub is_bool {
    my $val = shift;
    
    return(is_string($val) && $val =~ $BOOLPAT || 0);
}

sub is_uint {
    my $val = shift;

    return(is_string($val) && $val =~ $UINTPAT || 0);
}

sub is_sint {
    my $val = shift;
    
    return(is_string($val) && $val =~ $SINTPAT || 0); 
}

sub is_ufloat {
    my $val = shift;
    
    return(is_string($val) && $val =~ $UFLOATPAT || 0);
}

sub is_sfloat {
    my $val = shift;
    
    return(is_string($val) && $val =~ $SFLOATPAT || 0);
}

sub is_hexi {
    my $val = shift;
    
    return(is_string($val) && $val =~ $HEXIPAT || 0);
}

sub is_octal {
    my $val = shift;

    return(is_string($val) && $val =~ $OCTALPAT || 0);
}

sub is_char {
    my $val = shift;
    
    return(is_string($val) && length($val) == 1 || 0)
}

sub is_regex {
    my $val = shift;
    
    local $@ = undef;
    if (UNIVERSAL::isa($val,'Regexp') && eval {'' =~ /$val/; 1}) {
	return(!$@ || 0);
    } elsif (is_string($val) && eval {'' =~ /$val/; 1}) {
	return(!$@ || 0);
    }
    return 0;
}

sub is_code {
    my $val = shift;
    
    if (is_string($val)) {
        # this is potentially dangerous...
	local $SIG{__WARN__} = \&__stash_code_error;
	{
	    local $@     = undef;
	    $CODE_ERROR  = undef;
	    eval "sub { $val }";
	    $CODE_ERROR .= defined($CODE_ERROR) ? "\n$@" : $@ if $@;
	}
	debug("\n",$CODE_ERROR);

	return(!$CODE_ERROR || 0);
    }
    return 0;
}

sub is_scalarref {
    my $val = shift;
    
    return(ref($val) eq 'SCALAR' || 0);
}

sub is_stringref {
    my $val = shift;
    
    return(is_scalarref($val) && is_string($$val) || 0);
}

sub is_arrayref {
    my $val = shift;

    return(ref($val) eq 'ARRAY' || 0); 
}

sub is_hashref {
    my $val = shift;
    
    return(ref($val) eq 'HASH' || 0); 
}

sub is_coderef {
    my $val = shift;
    # this is the best we can do? passing a code-ref
    # means it is precompiled and we cannot check to 
    # verify that it was a successful compile until we
    # attempt to execute it, which may have its own
    # undesirable side-effects.
    return(ref($val) eq 'CODE' || 0);
}

sub is_object {
    my $val = shift;
    
    return(ref($val) && UNIVERSAL::can($val,'isa') || 0);
}

sub is_subclass {
    my ($val,$class) = @_;
    
    if ( ! defined($class)) {
	throw TypeError('No class name specified for test.');
    }
    return(ref($val) && UNIVERSAL::isa($val,ref($class)||$class) || 0);
}

sub is_instance {
    my ($val,$class) = @_;
    
    if ( ! defined($class)) { 
	throw TypeError('No class name specified for test.');
    }
    return(ref($val) eq (ref($class)||$class) || 0);
}

sub is_globref {
    my $val = shift;
    
    return(ref($val) eq 'GLOB' || 0)
}

sub is_file {
    my ($val,$tests) = @_;

    if ( ! (is_string($tests) && length($tests))) {
	throw TypeError('No file-test string specified for test.');
    }
    # see: http://perldoc.perl.org/perlfunc.html#Alphabetical-Listing-of-Perl-Functions
    if (is_globref($val) || is_string($val)) {
	# for some reason $t (below) is expected to be global, 
	# so temporarily disable strict vars. Disable it here
	# instead of inside the loop to be faster (hopefully)
	no strict 'vars';
	for(my $i = 0; $i < length($tests); ++$i) {
	    my $t = substr($tests,$i,1);
	    $VALID_TEST{$t}   || throw ValueError("Invalid file test '-$t'.");
	    eval "-$t '$val'" || return 0;
	}
	return 1;
    }
    return 0;
}


1;

__END__

