package Valkyr::Data::Type::Assert;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use lib '.';
use vars qw($VERSION @EXPORT_OK %EXPORT_TAGS $FALSE);

use Valkyr::Data::Type::Test;
use Valkyr::Error qw(throw report);
use Valkyr::Error::Exception qw(TypeError AssertionError);


BEGIN {
    $VERSION = eval '0.0.3';
    @EXPORT_OK = qw(is_scalar is_bool is_uint is_sint is_ufloat is_sfloat
                    is_string is_char is_regex is_code is_object is_stringref
                    is_arrayref is_hashref is_coderef is_file is_globref
                    is_subclass is_instance);
    %EXPORT_TAGS = ('all' => \@EXPORT_OK);
    $FALSE = qr/^(?:0|F|FALSE)$/i;
}

sub REFUTED {
    my ($val,$type,$func) = @_;
    $type //= 'unspecified';
    $val  //= 'undef';
    if (Valkyr::Data::Type::Test::is_coderef($func)) {
	syswrite(STDERR,"\n");
	report("Value \"$val\" is not expected type: $type.");
	&{$func};
	exit(1); # just in case
    } else {
        throw AssertionError("Value \"$val\" is not expected type: $type.");
    }
}

sub is_bool {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_bool($val)) {
        REFUTED( $val,'bool',$func )
    }
    
    return($val !~ $FALSE || 0);
}

sub is_uint {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_uint($val)) {
        REFUTED( $val,'unsigned int',$func )
    }
    return $val;
}

sub is_sint {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_sint($val)) {
        REFUTED( $val,'signed int',$func )
    }
    return $val;
}

sub is_ufloat {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_ufloat($val)) {
        REFUTED( $val,'unsigned float',$func )
    }
    return $val;
}

sub is_sfloat {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_sfloat($val)) {
        REFUTED( $val,'signed float',$func )
    }
    return $val;
}

sub is_string {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_string($val)) {
        REFUTED( $val,'string',$func )
    }
    return $val;
}

sub is_char {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_char($val)) {
        REFUTED( $val,'char',$func )
    }
    return $val;
}

sub is_hexi {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_hexi($val)) {
        REFUTED( $val,'char',$func )
    }
    return $val;
}

sub is_octal {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_octal($val)) {
        REFUTED( $val,'char',$func )
    }
    return $val;
}

sub is_regex {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_regex($val)) {
        REFUTED( $val,'regex',$func )
    }
    return $val;
}

sub is_code {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_code($val)) {
        REFUTED( $val,'(successfully compiling) code string',$func )
    }
    return $val;
}

sub is_object {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_object($val)) {
        REFUTED( $val,'object',$func )
    }
    return $val;
}

sub is_stringref {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_stringref($val)) {
        REFUTED( $val,'stringref',$func )
    }
    return $val;
}

sub is_arrayref {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_arrayref($val)) {
        REFUTED( $val,'arrayref',$func )
    }
    return $val;
}

sub is_hashref {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_hashref($val)) {
        REFUTED( $val,'hashref',$func )
    }
    return $val;
}

sub is_globref {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_globref($val)) {
        REFUTED( $val,'globref',$func )
    }
    return $val;
}

sub is_coderef {
    my ($val,$func) = @_;
    if ( ! Valkyr::Data::Type::Test::is_coderef($val)) {
        REFUTED( $val,'coderef',$func )
    }
    return $val;
}

sub is_file {
    my ($val,$attr,$func) = @_;

    Valkyr::Data::Type::Test::is_string($attr) || 
	throw TypeError('No file-test string specified for test.');

    if ( ! Valkyr::Data::Type::Test::is_file($val,$attr)) {
        REFUTED( $val,"file (modes=[$attr])",$func )
    }
    return $val;
}

sub is_subclass {
    my ($val,$attr,$func) = @_;

    Valkyr::Data::Type::Test::is_string($attr) || 
	throw TypeError('No class name specified for test.');

    if ( ! Valkyr::Data::Type::Test::is_subclass($val,$attr)) {
        REFUTED( $val,"subclass ($attr)",$func )
    }
    return $val;
}

sub is_instance {
    my ($val,$attr,$func) = @_;

    Valkyr::Data::Type::Test::is_string($attr) || 
	throw TypeError('No class name specified for test.');

    if ( ! Valkyr::Data::Type::Test::is_instance($val,$attr)) {
        REFUTED( $val,"instance ($attr)",$func )
    }
    return $val;
}


1;
