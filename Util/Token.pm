package Valkyr::Util::Token;

use 5.008;
use strict;
use warnings;

use Config;
use English;
use Exporter 'import';

use File::Spec;
use File::Basename;

use vars qw($VERSION @EXPORT_OK %EXPORT_TAGS);
use constant {
    ONCE     =>     'once',
    REFS     =>     'refs',
    EMPTY    =>         '',
    PACKAGE  =>       '::',
    TOPLEVEL => 'toplevel',
};

BEGIN {
    $VERSION     = eval '0.0.0'; # experimental
    @EXPORT_OK   = qw(__PROGRAM__ __FUNCTION__ __THREAD__ __BADGE__ __SUB__);
    %EXPORT_TAGS = ('all' => \@EXPORT_OK);

    no strict   REFS;
    no warnings ONCE;
    eval {
	require threads;
    };if ($EVAL_ERROR) {
	*__THREAD__ = sub () { 0 };
    } else {
	*__THREAD__ = \&threads::tid;
    }
}

sub __PROGRAM__ {
    # File::Spec->rel2abs() returns value of $HOME if $main::0 eq EMPTY,
    # which is not what we want, so just return undef.
    if ( ! defined( $main::0 ) || $main::0 eq EMPTY) {
	return '';

    } elsif ( $main::0 eq '-e' ) {
	my $suffix   = $Config{'_exe'};
	my $exepath  = $Config{'perlpath'};
	if ($OSNAME ne 'VMS') { 
	    $exepath =~ s/(?:$suffix)?$/$suffix/;
	}
	return File::Spec->rel2abs( $exepath );

    } else {
	return File::Spec->rel2abs( $main::0 );
    }
}


sub __BADGE__ {
    return '['.basename(__PROGRAM__).']';
}


sub __FUNCTION__ {
    my $func  = (CORE::caller(1))[3];
    
    if (defined($func) && $func =~ /^\(?eval/) {
	$func = 'eval';
    }
    return $func;
}


sub __SUB__ {
    my $func  = (CORE::caller(1))[3];

    if (!defined($func) || $func =~ /^\(?eval/) {
	return;
    } 
    no strict REFS;
    return \&{$func};
}


1;

__END__


=head1 NAME

Valkyr::Util::Token - 'tokens' for more readable code

=head1 SYNOPSIS

Import token-style subroutines:

B<NOTE>: C<Valkyr::Util::Token> does not export any of its subroutines
by default, the user must import them explicitly:

    use Valkyr::Util::Token qw(...);

The user may then treat them as Perl's built-in tokens:

    printf("Launching program: %s\n",__PROGRAM__);

    printf("My current subroutine is %s\n",__FUNCTION__); 

=head1 DESCRIPTION

This module contains "tokens" (actually pseudo-tokens) to access 
otherwise constant values that the programmer has no reason or 
desire to modify--such as the current subroutine name, full path
of the current program (for help docs and the like), or obtaining 
the current thread number (in a portable way)--but may desire to
use in his/her code frequently. The ultimate purpose of this module
is to increase the readability and maintainability of the code that
may prospectively C<use> this module.

Rather than litter code with calls to the cryptic C<$0>, it is
more explicit to use the C<__PROGRAM__> pseudo-token defined in this
package. This subroutine, when C<perl>'s C<-e> or C<-E> options are
enabled, outputs the full path of the Perl executable instead of the 
default C<-e> stored in C<$0>.

To obtain the name of the subroutine in the current frame, the
C<__FUNCTION__> pseudo-token makes maintaining code much simpler
by removing the annoyance of hard-coding the subroutine/function
name where the programmer needs it.
    
It is sometimes desirable to access the current thread ID in
which a particular piece of code is running (e.g. for stacktrace
dumps), however C<use>ing the L<threads> module may cause 
incompatibilities in instances of perl compiled without ithreads,
the C<__THREAD__> subroutines sorts this out for you.

Within this module is also a stand-in for the C<__SUB__> token for
use with Perls prior to v5.16.0. See the official Perl 
documentation for more information on L<__SUB__>.

B<NOTE>: The Valkyr::Util::Token module was designed with only a 
procedural interface.

=head1 SUBROUTINES

=head2 __PROGRAM__

  Usage    : my $program = __PROGRAM__;
  Function : Obtain the full path of the current executing program.
  Args     : None.
  Returns  : String.
  Comments : Has the same caveats as $0, but never takes the
             value "-e". May return undef if no program path defined. 
             
             
=head2 __FUNCTION__

  Usage    : my $subname = __FUNCTION__;
  Function : Return the name of the current subroutine/function.
  Args     : None.
  Comments : May return undef if no subroutine defined in the 
             current frame.

=head2 __THREAD__

  Usage    : my $thread_id = __THREAD__;
  Function : Return the current thread ID.
  Args     : None.
  Comments : Uses "threads" module to return the current thread ID
             from perls compiled with ithreads and returns 0 (the 
             "main" thread ID) from compiles that were not.

=head2 __SUB__

  Usage    : my $function_ref = __SUB__;
  Function : Return a reference to the current subroutine/function.
  Args     : None.
  Comments : Emulates the "__SUB__" token introduced in Perl v5.16.0;
             It is recommended to load this pseudo-token into one's
             script conditionally (see BUGS AND LIMITATIONS). May
             return undef if no subroutine defined in the current 
             frame.


=head1 INCOMPATIBILITIES

This module requires Perl 5.10.0 or higher.

=head1 SEE ALSO

L<caller>() and L<__SUB__>

=head1 AUTHORS

Jessen Bredeson <jessenbredeson@berkeley.edu>

=head1 BUGS AND LIMITATIONS

Please report bugs to the Author(s).

=over

=item * A potential source of bugs in user code may come from using these
pseudo-tokens (which are really just subroutines) with Perl's implicit
quoting when using the associative (C<=E<gt>>) operator or using the
pseudo-token as an hash key, e.g.:

    %foo = ( __THREAD__ => 1); # sets string "__THREAD__" as key
                       -or-
    $bar{ __THREAD__ } = 1; # same as above

To avoid this outcome, be explicit:

    %foo = ( __THREAD__() => 1);  # sets return value of __THREAD__ as key
                       -or-
    $bar{ __THREAD__() } = 1; # same as above

=back

=over

=item * In C<perl> compiles that are v5.16.0 or newer, importing this module's
C<__SUB__> pseudo-token may cause an error. It is therefore important
to load the module into your own script or module conditionally within
a C<BEGIN> block:

    BEGIN {
        eval "__SUB__";
        if ($@) {
            local $@ = undef;
            eval "use Valkyr::Util::Token '__SUB__'";
            die "could not load __SUB__" if $@;
        }
    }

=back

=head1 LICENSE AND COPYRIGHT

None yet. Don't plagiarize me, please.

=cut
