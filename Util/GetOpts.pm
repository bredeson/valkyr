package Valkyr::Util::GetOpts;

use strict;
use warnings;

use Valkyr::Data::Type::Test ':all';
use Valkyr::Util::Token '__FUNCTION__';
use Valkyr::Error qw(report throw);


use vars qw($VALID_SPEC_TYPE %VALID_SPEC_TEST);

BEGIN {
    %VALID_SPEC_TEST = (
	's' => \&Valkyr::Data::Type::Test::is_string,
	'i' => \&Valkyr::Data::Type::Test::is_sint,
	'o' => \&Valkyr::Data::Type::Test::is_octal,
	'x' => \&Valkyr::Data::Type::Test::is_hexi,
	'c' => \&Valkyr::Data::Type::Test::is_char,
	'C' => \&Valkyr::Data::Type::Test::is_code,
	'f' => \&Valkyr::Data::Type::Test::is_sfloat,
	'F' => \&Valkyr::Data::Type::Test::is_file,
     );
    $VALID_SPEC_TYPE = join('',keys(%VALID_SPEC_TEST));
}

sub parse_switch {
    my ($opts);
}

sub parse_option {

}

sub parse_option_array {

}

sub parse_option_hash {

}

sub parse_options {
    
}	

sub parse_spec {
    my $specstring = shift;
    assert {is_string($specstring)} 
        'Must pass an option spec-string to ',__FUNCTION__;
    
    $specstring =~ s/^\s+|\s+$//g;
    if ($specstring =~ /^([^=\s]+)(?:=(.*))?/) {
	
	my $spectype = $2;
	my @optnames = split('|',$1);
	
	if ( ! @optnames ) { 
	    throw ValueError("Malformed option spec \"specstring\": ",
			     'No flag name specified.')
	}
	    
	if ($spectype) { # requires configuring
	    if ($spectype =~ /^([$VALID_SPEC_TYPE])(?:\[(.*)\])?$/) {
		for my $name (@optnames) {
		    $spec{$name} = exists($spec{$name})
			? throw ValueError("Option name multiply-",
					   "defined: $name\n")
			: $VALID_SPEC_TEST{$1}
		}
	    } else {
		throw ValueError("Malformed option spec \"$optspec\": ",
				 "Invalid spec type \"$1\"");
	    }
	} else { # is a switch 
	    parse_switch();
	}
    } else {
	throw ValueError("Malformed option spec: $optspec");
    }
	    
}	    

sub getopts {
    my ($args,$hash) = @_;
    
    my %INPUT_TYPES = ();
    
    
}

package Valkyr::Util::GetOpts::option;

sub __spec_types {
    my $self = shift;
    $self->{'__spec_types'} = shift if @_;
    return $self->{'__spec_types'};
}

sub __type_test {
    my $self = shift;
    $self->{'__type_test'} = shift if @_;
    return $self->{'__type_test'};
}

sub __set_option {
    my $self = shift;
    $self->{'__option'} = shift if @_;
}

sub get_option { return shift->{'__option'} }

sub __set_value {
    my $self = shift;
    $self->{'__option'} = shift if @_;
}

sub get_option { return shift->{'__option'} }


sub validate_arg {
    my $self = shift;

    exists($self->
	}


sub __new {
    my ($ref,@args) = @_;
    
    my $class = ref($ref) || $ref;
    my $self  = bless({},$class);
    
    

1;


getopts('o|out=F[rwx]:t|thresh=i[+0-]:a|all:f|float=f[+0]:',)


