package Valkyr::Error::Exception;

use 5.010;
use strict;
use warnings;

use base qw(Valkyr::Class::Base);

use Valkyr::Util::Token qw(__BADGE__ __FUNCTION__);
use Valkyr::Class::Constants qw(REFS SUBS ONCE RECURSION);
use Valkyr::Error::Constants qw(BASECLASS);
use Valkyr::Error::Exception::BaseException;

use vars qw($SYMTABLE $HEIRARCHY $EXPORTED $VALID_FUNCNAME $VALID_DATATYPE 
            $UNDEFINED $INVALID $REDEFINED $BADNAME $BADEXPORT $CIRCULAR 
            $ISA $PKG $ROOTCLASS $EXPORTABLE $DEBUG);

BEGIN { 
    no strict   REFS;
    no strict   SUBS;
    no warnings ONCE;
    $ISA       = Valkyr::Class::Constants::ISA;
    $PKG       = Valkyr::Class::Constants::PKG;
    $SYMTABLE  = \%{ __PACKAGE__.$PKG };
    $UNDEFINED = __BADGE__." No %s defined for %s.\n";
    $INVALID   = __BADGE__." Invalid %s datatype: %s.\n";
    $REDEFINED = __BADGE__." Subroutine '%s' redefined.\n";
    $BADNAME   = __BADGE__." Invalid character(s) in function name: %s.\n";
    $BADEXPORT = __BADGE__." '%s' is not exported by the %s module.\n";
    $CIRCULAR  = __BADGE__." Recursive inheritance detected in package '%s'.\n";
    $VALID_FUNCNAME = qr/^(?:[[:alpha:]]\w*$PKG)*[[:alpha:]_]\w*$/;
    $VALID_DATATYPE = qr/^(?:SCALAR|ARRAY|HASH)$/;
}

sub __join {
    my $self   = shift;
    my $class  = $self->__validate_symbol( shift );
    my $symbol = $self->__validate_symbol( shift );
    
    return $symbol !~ /$PKG/            ? $class.$PKG.$symbol :
	   $symbol =~ /^$class$PKG\w+$/ ? $symbol             : 
	   die( sprintf($BADEXPORT,$symbol,$class) );
}

sub __validate_symbol {
    my $self   = shift;
    my $symbol = shift;

    defined( $symbol ) && $symbol =~ $VALID_FUNCNAME ||
	die( sprintf($BADNAME, $symbol||'undef') );
    
    return $symbol
}

sub __validate_exports {
    my $self   = shift;
    my $symbol = shift;
    my $type   = ref( $symbol ) || ref(\$symbol );
    
    # check that the symbols are from this package
    return map { 
	$self->__join( __PACKAGE__,$_ ); 
	die( sprintf($BADEXPORT, $_,__PACKAGE__) )
	    if ! $SYMTABLE->{ $_ }; 
	$_;
    } ( $type eq 'SCALAR' ? $symbol            :
        $type eq 'ARRAY'  ? @{$symbol}         :
        $type eq 'HASH'   ? keys( %{$symbol} ) :
        die( sprintf($INVALID, 'export',$type)) );
}

sub __define_class {
    my $exporter = shift;
    my $subclass = $exporter->__join( __PACKAGE__,shift );
    my $inheritance = shift;

    my $type = defined( $inheritance ) 
	? ( ref( $inheritance ) || ref(\$inheritance ) ) 
	: die( sprintf($INVALID, 'inheritance','undef') );

    no strict   REFS;
    no warnings ONCE;

    @{ $subclass.$ISA } = map { $exporter->__join( __PACKAGE__,$_ ) }
	$type eq 'SCALAR' ? $inheritance            :
	$type eq 'ARRAY'  ? @{$inheritance}         :
	$type eq 'HASH'   ? keys( %{$inheritance} ) :
	die( sprintf($INVALID, 'inheritance',$type) );
    # maybe should perform symtable lookup here instead: more reliable?
    return 1;
}

sub __define_symbol {
    my $exporter = shift;
    my $symbol   = $exporter->__join( __PACKAGE__,shift );

    $exporter->CAN( $symbol ) && return 1;

    eval "sub $symbol { return $symbol->SUPER::new(\@_) }";

    return 1;
}

sub __export_symbol {
    my $exporter = shift;
    my $importer = $exporter->__validate_symbol( shift );
    my $symbol   = $exporter->__validate_symbol( shift );
    
    if ($EXPORTED->{ $exporter->__join( $importer,$symbol ) }) {
	syswrite(STDERR, sprintf($REDEFINED,$symbol) );
    }

    no strict   REFS;
    no warnings ONCE;

    *{ $exporter->__join( $importer,$symbol ) } 
        = \&{ $exporter->__join( $exporter,$symbol ) };

    return 1;
}

sub __graft_subtree {
    my ($self,$classtree,$subtree,$root) = @_;
    
    defined( $root ) || die( sprintf($UNDEFINED, 'node','grafting') );

    my $datatype  = ref( $subtree->{$root} ) || ref(\($subtree->{$root}) );
    my @leafnodes = 
	$datatype eq 'HASH'   ? keys( %{$subtree->{$root}} ) :
	$datatype eq 'ARRAY'  ? @{$subtree->{$root}}         : 
	$datatype eq 'SCALAR' ? ($subtree->{$root})          :
        die( sprintf($UNDEFINED, 'leaf','grafting') );

    my $n_grafted   = 0;
    while (my $node = shift @leafnodes) {
	if (! exists( $classtree->{$root}->{$node} )) {
	    die( sprintf($CIRCULAR,$self->__join( __PACKAGE__,$root )))
		if UNIVERSAL::isa( $self->__join( __PACKAGE__,$root ),
				   $self->__join( __PACKAGE__,$node ) );

	    $self->__define_class( $node,$root );
	    $self->__define_symbol( $node );
	    $n_grafted++;
	    $classtree->{$root}->{$node} = {};
	}
	$n_grafted += $self->__graft_subtree( $classtree->{$root},$subtree->{$root},$node )
	    if $datatype eq 'HASH';
    }
    return $n_grafted;
}

sub __traverse_subtree {
    my ($self,$classtree,$subtree,$node) = @_;
    
    # $classtree is assumed to be a pre-defined hash-ref tree 
    # $subtree is a user-defined tree of arbitrary depth, the base node(s)
    #  may exist anywhere in $classtree. This allows the user of this
    #  package to concisely define a new sub-class without having to 
    #  explictly define the full, resolved path to the sub-class (terminal 
    #  node) of interest.

    # If the node exists in the tree, initialize grafting at that node:
    if (exists( $classtree->{$node} )) {
	my $type = ref( $subtree->{$node} ) || ref(\($subtree->{$node}) );
        die( sprintf($INVALID, 'derived-class',$type) )
	    if $type !~ $VALID_DATATYPE;
    
        return $self->__graft_subtree( $classtree,$subtree,$node );
    # we recursively check all nodes in $classtree for the node
    #  of interest (this is time-expensive, but necessary)
    } else { 
        for my $sibnode (keys( %{$classtree} )) {
	    # Depth-first traversal of all sibling nodes:
	    if ($self->__traverse_subtree( $classtree->{$sibnode},$subtree,$node )) {
	        return 1;
	    }
        }
        # if we have fallen out of the 'for' loop, we have traversed 
        #  the entire class tree and have found no base-class node.
    }
    return 0; # fallback status
}

sub __export_subtree {
    my ($exporter,$importer,$subtree,$root) = @_;
    
    my $exported = 0;
    my $datatype = ref( $subtree->{$root} ) || ref(\($subtree->{$root}) );

    if ($datatype eq 'HASH') {
	for my $subnode (keys( %{$subtree->{$root}} )) {
	    $exported += $exporter->__export_symbol( $importer,$subnode );
	    $exported += $exporter->__export_subtree( $importer,$subtree->{$root},$subnode );
	}
    } else {
	my @leafnodes = 
	    $datatype eq 'ARRAY'  ? @{$subtree->{$root}} : 
	    $datatype eq 'SCALAR' ? ($subtree->{$root})  :
	    die( sprintf($INVALID, 'node',$datatype) );
	
	for my $node ( @leafnodes ) {
	    $exported += $exporter->__export_symbol( $importer,$node );
	}
	return $exported;
    }
    return $exported;
}

sub export {
    my ($exporter,$importer,@exports) = @_;
    
    # PLAN: Distinguish between lists of strings (symbols ALREADY defined)
    #   and hash-refs (symbols TO BE defined) containing prospective symbols 
    #   and their inherited parents. Parents should ultimately derive from
    #   Valkyr::Error::Exception::BaseException.
    
    # Divide user input into a list of natively defined symbols for 
    #   export, and a look-up table of symbols to be defined.  Iterate over 
    #   requested symbols (the list above) and verify that the user passed 
    #   valid symbol names

    if (! @exports) { return }

    {
	no strict REFS;
	$EXPORTED ||= \%{ $importer.$PKG };
    }
    my @exportables;
    my %exportables;

    while (my $symbol = shift @exports) {
	if (ref($symbol) eq 'HASH') {
	    # merge multiple hash-refs if the user passes more than one
	    map { $exportables{ $_ } = $symbol->{ $_ } } 
	        $exporter->__validate_exports( $symbol );
	} else {
	    push @exportables,
	        $exporter->__validate_exports( $symbol );
	}
    }
    # THEN: Recursively traverse look-up table (defined above), verifying
    #   parentage and defining new symbols
    for my $symbol (keys( %exportables )) {
	$exporter->__traverse_subtree( $HEIRARCHY,\%exportables,$symbol );
    }
    # LASTLY: export validated symbols
    for my $symbol ( @exportables ) {
	$exporter->__export_symbol( $importer,$symbol );
    }
    for my $symbol (keys( %exportables )) {
	$exporter->__export_subtree( $importer,\%exportables,$symbol );
    }

    return 1;
}

sub import {
    my $exporter = shift;
    my $importer = (caller(0))[0];
    
    # lifted from Badger::Exporter
    strict->import();
    warnings->import();
    
    syswrite(STDERR,"Importer: $importer :: @_\n") if $DEBUG;

    $exporter->export( $importer,@_ );
}

BEGIN {
    $HEIRARCHY = {
	'BaseException' => {
	    'KeyboardInterrupt' => {},
	    'Exception'         => {
		'StandardError' => {
		    'ArithmaticError' => {
			'FloatingPointError' => {},
			'OverflowError'      => {},
			'ZeroDivisionError'  => {},
		    },
		    'AssertionError'   => {},
		    'AttributeError'   => {},
		    'EnvironmentError' => {
			'IOError'  => {},
			'OSError'  => {
			    'WindowsError' => {},
			    'VMSError'     => {},
			},
		    },
		    'EOFError'     => {},
		    'RequireError' => {},
		    'LookupError'  => {
			'IndexError' => {},
			'KeyError'   => {},
		    },
		    'MemoryError'  => {},
		    'NameError'    => {
			'UnboundLocalError' => {},
		    },
		    'ReferenceError' => {},
		    'RuntimeError'   => {
			'NotImplementedError' => {},
		    },
		    'SyntaxError'  => {},
		    'SystemError'  => {},
		    'TypeError'    => {},
		    'ValueError'   => {
			'UnicodeError' => {
			    'UnicodeDecodeError'    => {},
			    'UnicodeEncodeError'    => {},
			    'UnicodeTranslateError' => {},
			},
		    },
		},
		'Warning' => {
		    'DeprecationWarning'        => {},
		    'PendingDeprecationWarning' => {},
		    'RuntimeWarning'            => {},
		    'SyntaxWarning'             => {},
		    'UserWarning'               => {},
		    'FutureWarning'             => {},
		    'RequireWarning'            => {},
		    'UnicodeWarning'            => {},
		    'BytesWarning'              => {},
		},
	    },
	},
    };
}

BEGIN {
    # START: by defining the methods listed in $HEIRARCHY and stash their 
    #   names in a look-up table of defined symbols (done automatically by 
    #   $SYMTABLE)
    local $@ = undef;
    eval {
	no warnings RECURSION;
	__PACKAGE__->__define_symbol( BASECLASS );
	__PACKAGE__->__traverse_subtree( {BASECLASS() => {}},$HEIRARCHY,BASECLASS );
    };
    if ($@) {
	die( __BADGE__.' '.__PACKAGE__." initialization failed:\n$@" );
    }
}

1;

__END__
