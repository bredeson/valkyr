package Valkyr::Error::Base;

use 5.007_003;

use strict;
use warnings;

use Carp;
use Exporter 'import';

use vars qw(@EXPORT_OK %EXPORT_TAGS);

use Valkyr::Error::Constants 'TOPLEVEL';

BEGIN {
    @EXPORT_OK   = qw(__stacktrace);
    %EXPORT_TAGS = ('all' => \@EXPORT_OK);
}


sub __stacktrace {
    if (defined($_[0]) && UNIVERSAL::isa( $_[0], __PACKAGE__ )) { shift }

    my $level = shift || 0;

    $level++;

    my @STACK;
    my $fxn   = TOPLEVEL;
    my @line  = reverse( split(/\n/, Carp::longmess ));
    for(my $i = 0; $i < @line - $level; ++$i) {
        $line[$i] =~ s/^\s+//;
	$line[$i] =~ s/\(eval\s*\d*\)\[(.*):(\d+)\](?: line \d+)?/$1 line $2/;
        if ($line[$i] =~ /^([\w\:]+).*?\s+called\s+at\s+(.*?)\s+line\s+(\d+)/ ||
            $line[$i] =~ /^(require\s+0)\s+called\s+at\s+(\S+)\s+line\s+(\d+)/) {

            unshift(@STACK, sprintf('  at %s(%s:%u)', $fxn, $2, $3 ));
            $fxn  = $1;
        }
    }
    
    return @STACK;
}


1;
