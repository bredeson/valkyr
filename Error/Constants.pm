package Valkyr::Error::Constants;

use Exporter 'import';
use vars qw($EXPORT_OK %EXPORT_TAGS);

BEGIN {
    @EXPORT_OK   = qw(TOPLEVEL DEFAULT_STACKWIDTH ROOTCLASS BASECLASS);
    %EXPORT_TAGS = ('all' => \@EXPORT_OK);
    *ROOTCLASS   = sub () { 'Valkyr::Error::Exception' };
    *BASECLASS   = sub () { 'BaseException' };
    *TOPLEVEL    = sub () { 'main' };
    *DEFAULT_STACKWIDTH = sub () { 119 };
}

1;
