package Valkyr::Error::SigHooks;

use 5.007_003; # safe signals
use strict;
use warnings;

use vars qw(@EXPORT);
use base qw(Valkyr::Error::Base);

use Valkyr::Error qw(:all :dev);
use Valkyr::Error::Base ':constants';
use Valkyr::Error::Exception ':all';
use Valkyr::Util::Token qw(__BADGE__);

BEGIN {
    no strict REFS;
    # fatals:
    *CORE   = sub () {  'CORE'   };
    *ABRT   = sub () { 'SIGABRT' };
    *BUS    = sub () { 'SIGBUS'  };
    *EMT    = sub () { 'SIGEMT'  };
    *FPE    = sub () { 'SIGFPE'  };
    *ILL    = sub () { 'SIGILL'  };
    *QUIT   = sub () { 'SIGQUIT' };
    *SEGV   = sub () { 'SIGSEGV' };
    *SYS    = sub () { 'SIGSYS'  };
    *TRAP   = sub () { 'SIGTRAP' };
    # non-fatals:
    *NORMAL = sub () { 'TERMINATE' };
    *KILL   = sub () {  'SIGKILL'  };
    *TERM   = sub () {  'SIGTERM'  };
    *HUP    = sub () {  'SIGHUP'   };
    *INT    = sub () {  'SIGINT'   };
    *PIPE   = sub () {  'SIGPIPE'  };
    # 
    *TTOU  = sub () { 'SIGTTOU' };
    *TTIN  = sub () { 'SIGTTIN' };
}
    

sub import {
    my $pkg = shift;

    $main::SIG{'__WARN__'} = \&Valkyr::Error::warn;
    $main::SIG{'__DIE__'}  = \&Valkyr::Error::throw;
    no strict 'subs';
    for my $sig (qw(ABRT BUS FPE ILL QUIT SEGV SYS TRAP)) {
	$main::SIG{$sig}   = sub {
	    syswrite(STDERR,"\n".__format_stacktrace(NORMAL,
			    EXCEPTION_BASECLASS()->new('Caught ',&{$sig},'.')));
	}
    };

    return 1;
}

1;

# From: http://linux.about.com/od/commands/l/blcmdl7_signal.htm

# Action:
# Term - Terminates program, don't worry about loose ends
# Ign  - Ignore
# Core - Terminate and dump core
# Stop - Shut everything down gracefully

# Several signal numbers are architecture dependent, as indicated in the "Value" 
# column. (Where three values are given, the first one is usually valid for 
# alpha and sparc, the middle one for i386, ppc and sh, and the last one for 
# mips. A - denotes that a signal is absent on the corresponding architecture.


# SIGNAL EXIT(N) ACTION DESCRIPTION
# ==============================================================================
# POSIX.1 Standard:
# ------------------------------
# SIGINT         2     Term  Interrupt from keyboard
# SIGQUIT        3     Core  Quit from keyboard
# SIGILL         4     Core  Illegal Instruction
# SIGABRT        6     Core  Abort signal from abort(3)
# SIGFPE         8     Core  Floating point exception
# SIGKILL        9     Term  Kill signal
# SIGSEGV       11     Core  Invalid memory reference
# SIGPIPE       13     Term  Broken pipe: write to pipe with no readers
# SIGALRM       14     Term  Timer signal from alarm(2)
# SIGTERM       15     Term  Termination signal
# SIGUSR1    30,10,16  Term  User-defined signal 1
# SIGUSR2    31,12,17  Term  User-defined signal 2
# SIGCHLD    20,17,18  Ign   Child stopped or terminated
# SIGCONT    19,18,25  Continue if stopped
# SIGSTOP    17,19,23  Stop  Stop process
# SIGTSTP    18,20,24  Stop  Stop typed at tty
# SIGTTIN    21,21,26  Stop  tty input for background process
# SIGTTOU    22,22,27  Stop  tty output for background process

# POSIX 1003.1 and SUSv2 and SUSv3:
# ------------------------------
# SIGPOLL              Term  Pollable event (Sys V). Synonym of SIGIO
# SIGPROF    27,27,29  Term  Profiling timer expired
# SIGSYS     12, -,12  Core  Bad argument to routine (SVID)
# SIGTRAP        5     Core  Trace/breakpoint trap
# SIGURG     16,23,21  Ign   Urgent condition on socket (4.2 BSD)
# SIGVTALRM  26,26,28  Term  Virtual alarm clock (4.2 BSD)
# SIGXCPU    24,24,30  Core  CPU time limit exceeded (4.2 BSD)
# SIGXFSZ    25,25,31  Core  File size limit exceeded (4.2 BSD)

# POSIX 1003.1-2001:
# ------------------------------
# SIGEMT      7, -, 7  Term
# SIGSTKFLT   -,16,-   Term  Stack fault on coprocessor (unused)
# SIGIO      23,29,22  Term  I/O now possible (4.2 BSD)
# SIGCLD      -, -,18  Ign   A synonym for SIGCHLD
# SIGPWR     29,30,19  Term  Power failure (System V)
# SIGINFO    29, -, -  Term  A synonym for SIGPWR
# SIGLOST     -, -, -  Term  File lock lost
# SIGWINCH   28,28,20  Ign   Window resize signal (4.3 BSD, Sun)
# SIGUNUSED   -,31, -  Term  Unused signal (will be SIGSYS)

