

sub try (&;$) { eval { $_[0]->() }; $Valkyr::Error::THROWN = $@ }

sub type (\[$@%&*]) { 
    $type = $_[0]; 
    if (ref($type) eq 'REF') { 
	return ref($$type).'REF';
    } else { 
	return ref(\$_);
    } 
}

sub try (&;$) {
    my ($try,@stack) = @_;
    
    my @result;
    my $wantarray = wantarray();
    
    {
	local $@ = undef;
	local $Valkyr::Error::THROWN = undef;
	eval { 
	    if ($wantarray) {
		(@result) = &$try;
	    } elsif (defined $wantarray) {
		$result[0] = &$try;
	    } else {
		&$try;
	    }
	};
	
    }

	
}

sub except (&;@) { 
    my $except = shift; 
    my $stack  = shift || []; 
    unshift @$stack,[ __SUB__ , $except ]; 

    return $stack;
}
 

sub otherwise (&;@) {
    my $other = shift; 
    my $stack = shift || []; 
    unshift @$stack,[ __SUB__ , $other ]; 

    return $stack;
}

sub finally (&) {
    my $fin = shift;
    return [ __SUB__ , $fin ];
}
