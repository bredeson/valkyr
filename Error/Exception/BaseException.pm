package Valkyr::Error::Exception::BaseException;

use 5.008001;

use strict;
# use warnings;

use base qw(Valkyr::Class::Base);

use Valkyr::Util::Token qw(__THREAD__);
use Valkyr::Error::Base qw(__stacktrace);
use Valkyr::Error::Constants qw(BASECLASS);
use Valkyr::Class::Constants qw(EMPTY);


sub __set_class {
    my $self = shift;
    $self->{'__class'} = shift if @_;
}

sub __set_thread {
    my $self = shift;
    $self->{'__thread'} = shift if @_;
}

sub __set_stacktrace {
    my $self = shift;
    $self->{'__stacktrace'} = shift if @_;
}

sub __initialize {
    my ($self,@args) = @_;

    $self->__set_class(
	$self->DERIVED_NAME() eq BASECLASS ? EMPTY : $self->DERIVED_NAME() );
    $self->__set_thread( __THREAD__ );
    $self->__set_stacktrace( __stacktrace(2) );
    $self->message( join(EMPTY, @args,EMPTY) );

    return $self;
}

# -----------------------------------------------------------------------------
# Public Methods:
# -----------------------------------------------------------------------------

sub class  { return shift->{'__class'} }

sub thread { return shift->{'__thread'} }

sub stacktrace { return shift->{'__stacktrace'} }

sub message {
    my $self = shift;
    $self->{'__message'} = shift if @_;
    return $self->{'__message'};
}


1;

__END__
