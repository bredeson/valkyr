# Copyright (c)2013. The Regents of the University of California (Regents).
# All Rights Reserved. Permission to use, copy, modify, and distribute this
# software and its documentation for educational, research, and
# not-for-profit purposes, without fee and without a signed licensing
# agreement, is hereby granted, provided that the above copyright notice,
# this paragraph and the following two paragraphs appear in all copies,
# modifications, and distributions. Contact The Office of Technology
# Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley, CA
# 94720-1620, (510) 643-7201, for commercial licensing opportunities.

# Created by Jessen Bredeson, Department of Molecular and Cell Biology, 
# University of California, Berkeley.

# IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
# SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

# REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
# HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
# MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.


package Valkyr::FileUtil::IO;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use IO::File;
use IO::Handle;
use IO::Compress::Gzip '$GzipError';
use IO::Compress::Bzip2 '$Bzip2Error';
use IO::Uncompress::Gunzip '$GunzipError';
use IO::Uncompress::Bunzip2 '$Bunzip2Error';

use Valkyr::Error 'throw';
use Valkyr::Error::Exception qw(ValueError IOError);
use Valkyr::Class::Constants qw(EMPTY);
use Valkyr::Data::Type::Assert qw(is_uint is_string);

use vars qw(@EXPORT);

# file IO constants
sub READ  () { return 'r' }
sub WRITE () { return 'w' }
sub STDIO () { return '-' }
sub COMPRESS () { return 1 }
sub NOSTREAM () { return 2 }


BEGIN {
    @EXPORT    = qw(READ WRITE STDIO NOSTREAM COMPRESS open is_older);
}

my $GZIP_SUFFIX_PAT = qr/[.\-](?:Z|g?z)$|_z$/;
my $BZIP_SUFFIX_PAT = qr/\.(?:t?bz2?|bzip2)$/;


sub is_older {
    my $file1 = is_string(shift);
    my $file2 = is_string(shift);
    
    if (! Valkyr::Data::Type::Test::is_file($file1,'e')) {
	throw("File does not exist: $file1");
    }
    if (! Valkyr::Data::Type::Test::is_file($file2,'e')) {
	throw("File does not exist: $file2");
    }
    my $file1age = -M $file1;
    my $file2age = -M $file2;

    if ($file1age > $file2age) {
	return -1;
    } elsif ($file1age < $file2age) {
	return  1;
    } else {
	return  0;
    }
}
    
    
sub open {
    my $file = is_string(shift);
    my $mode = is_string(shift||READ);
    my $opts = is_uint(shift||0);
    
    # 1 = gzip compress (or uncompress)
    # 2 = disallow STDIN
    
    
    if (defined($mode)) { $mode = lc($mode) }
    if ($file eq EMPTY) {
	throw IOError('Null file name disallowed for ',
		      $mode eq WRITE ? 'write.' : 'read.');
    }
    
    if (($mode ne READ) && ($mode ne WRITE)) { 
	throw ValueError("Invalid file mode '$mode'.");
    }
        
    if ($mode eq WRITE) {
	if (($opts & 0x2) && ($file eq STDIO)) {
	    throw IOError("Cannot open '",STDIO,"' for writing: STDOUT disallowed.");
	}
	if ($file =~ $GZIP_SUFFIX_PAT || $opts & 0x1) {
	    my $GzipError = EMPTY;
	    return IO::Compress::Gzip->new($file) || # '-' for stdout
		throw IOError("Cannot open '$file' for writing: $GzipError.");
	    
	} elsif ($file =~ $BZIP_SUFFIX_PAT) {
	    my $Bzip2Error = EMPTY;
	    return IO::Compress::Bzip2->new($file) || # '-' for stdout
		throw IOError("Cannot open '$file' for writing: $Bzip2Error.");
	    
	} else {
	    return( ($file eq STDIO
	    	? IO::Handle->new_from_fd(\*STDOUT, WRITE)
	    	: IO::File->new($file, WRITE)) ||
		    throw IOError("Cannot open '$file' for writing: .")); 
	}
	
    } else {
	if (($opts & 0x2) && ($file eq STDIO)) {
	    throw IOError("Cannot open '",STDIO,"' for reading: STDIN disallowed.");
	}
	if ($file =~ $GZIP_SUFFIX_PAT || $opts & 0x1) {
	    my $GunzipError = EMPTY;
	    return IO::Uncompress::Gunzip->new($file, 'MultiStream' => 1) || # '-' for stdin
		throw IOError("Cannot open '$file' for reading: $GunzipError.");

	} elsif ($file =~ $BZIP_SUFFIX_PAT) {
	    my $Bunzip2Error = EMPTY;
	    return IO::Uncompress::Bunzip2->new($file, 'MultiStream' => 1) ||
		throw IOError("Cannot open '$file' for reading: $Bunzip2Error.");

	} else {
	    return( ($file eq STDIO
		? IO::File->new_from_fd(\*STDIN,READ)
		: IO::File->new($file,READ)) ||
		    throw IOError("Cannot open '$file' for reading: reason unknown."));
	}
    }
}


1;


=head1 NAME

Valkyr::FileUtil::IO - Open files for reading and writing

=head1 SYNOPSIS

Open uncompressed files for reading:

    use Valkyr::FileUtil::IO; # open() imported by default
    my $in_fh  = open("infile.txt",'r')

Gzip-/Bzip2-compressed file types recognized automatically:

    my $in_fh  = open("infile.gz",'r')

Open a filehandle to STDOUT to write Gzip-compressed output:

    my $out_fh = open('-','w',1)


=head1 DESCRIPTION

This module was written to standardize/create a uniform interface
for opening filehandles for reading and writing to compressed or
uncompressed files.

=head1 CONSTANTS
- exported by default.

=head2 READ

  Purpose : Convenience for use with open()
  Returns : 'r'

=head2 WRITE

  Purpose : Convenience for use with open()
  Returns : 'w'

=head2 STDIO

  Purpose : Convenience for use with open()
  Returns : '-'

=head2 COMPRESS

  Purpose : Convenience for use with open()
  Returns : 1

=head2 NOSTREAM

  Purpose : Convenience for use with open()
  Returns : 2

=head1 SUBROUTINES

=head2 open()

  Usage   : open(<FILE> [,<MODE>][,<OPTS>]) 
  Purpose : Standardized file-open (read and write) system for 
             uncompressed, and Gzip- and Bzip2-compressed files.
  Args    : open() handles the following positional arguments:
            FILE : a filename or '-' for STDIN (for read) 
              or STDOUT (for write).
            MODE : may be 'r' for read, 'w' for write.
            OPTS : bitwise test:
              0x1 for compressed input/output, 
              0x2 for disallowing STDIN
  Returns : IO::Handle object

=head1 AUTHORS

Jessen Bredeson <jessenbredeson@berkeley.edu>

=head1 BUGS AND LIMITATIONS


=cut
