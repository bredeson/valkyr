package Valkyr::FileUtil::Counter;

use strict;
use warnings;
use Exporter 'import';

use Valkyr::Util::Token '__BADGE__';
use Valkyr::Error qw(report throw);
use Valkyr::Error::Exception qw(TypeError);
use Valkyr::Data::Type::Assert qw(is_uint is_ufloat is_sfloat is_string is_hashref);
use Valkyr::Math::Units::HumanReadable 'abbrev';

use vars qw(@EXPORT_OK);

my $CONFIGURED = 0;
my %USER_PARAM;
my %DEFAULT_PARAM = (
    'COUNT'       => 0,
    'PREFIX'      => '',
    'INCREMENT'   => 1,
    'GRANULARITY' => 1000,
    'MULTIPLIER'  => 1,
    );
@EXPORT_OK = qw(configCounter incrementCounter resetCounter progressCounter progressReport);


sub configCounter {
    my $param = is_hashref(shift);
    
    for my $key (keys(%$param)) {
	my $KEY = uc($key);
	$KEY =~ s/^-+//;
	if (exists($DEFAULT_PARAM{$KEY})) {
	    if ($KEY eq 'PREFIX') {
		$DEFAULT_PARAM{$KEY} = is_string($param->{$key}) || $DEFAULT_PARAM{$KEY};
	    } else {
		$DEFAULT_PARAM{$KEY} = is_ufloat($param->{$key}) || $DEFAULT_PARAM{$KEY};
	    }
	} else {
	    throw TypeError("Invalid parameter: $key");
	}
    }
    is_uint($DEFAULT_PARAM{'GRANULARITY'});

    return resetCounter();
}


sub resetCounter {
    $USER_PARAM{'GRANULARITY'} = $DEFAULT_PARAM{'GRANULARITY'};
    $USER_PARAM{'MULTIPLIER'}  = $DEFAULT_PARAM{'MULTIPLIER'};
    $USER_PARAM{'COUNT'}       = $DEFAULT_PARAM{'COUNT'};
    $USER_PARAM{'INCREMENT'}   = $DEFAULT_PARAM{'INCREMENT'};
    $USER_PARAM{'PREFIX'}      = $DEFAULT_PARAM{'PREFIX'};

    $CONFIGURED = 1;

   return 1;
}

	
sub incrementCounter {
    $CONFIGURED || resetCounter();

    my $i = is_sfloat(shift // $USER_PARAM{'INCREMENT'});
    $USER_PARAM{'COUNT'} += $i;
    return 1;
}


sub progressReport {
    $CONFIGURED || resetCounter();

    report(sprintf('%s%6s',$USER_PARAM{'PREFIX'},
		   sprintf('%-4s',abbrev($USER_PARAM{'COUNT'}||0,1))));
}


sub progressCounter {  
    $CONFIGURED || resetCounter();

    if (! ($USER_PARAM{'COUNT'} % $USER_PARAM{'GRANULARITY'})) {
	if (($USER_PARAM{'COUNT'} / $USER_PARAM{'GRANULARITY'}) >= $USER_PARAM{'MULTIPLIER'}) {
	    $USER_PARAM{'GRANULARITY'} *= $USER_PARAM{'MULTIPLIER'};
	}
	progressReport();
    }
    incrementCounter(@_);
}


1;
