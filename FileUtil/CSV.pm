package Valkyr::FileUtil::CSV;

use strict;
use warnings;
use FileHandle;
use Valkyr::FileUtil::IO;
use Valkyr::Error 'throw';
use Valkyr::Error::Exception qw(ValueError Exception IOError);

my $ESCAPE_CHARACTER = qr/^"$/;
my $QUOTE_CHARACTER = qr/^\"$/;
my $COMMA_CHARACTER = qr/^,$/;
my $WHITESPACE_LINE = qr/^\s+$/;
my $CRLF_CHARACTER = qr/^[\r\n]+$/;

sub new {
    my $class = shift;
    my $file  = shift;
    my $self  = {};
    if (! defined($file)) {
	throw ValueError("new() requires file name or FileHandle object");
    }
    if (UNIVERSAL::isa($file, 'GLOB')) { # A FileHandle or GLOB
	$self->{'__fh'} = FileHandle->new_from_fd($file, 'r') || 
	    throw IOError("Could not open from FileHandle");
    } else {
	$self->{'__fh'} = open($file, 'r')
    }

    return bless($self, $class);
}


sub __extract_and_cleanup_field {
    my $self  = shift;
    my $field = shift;  # ref to fields array
    my $line  = shift;  # ref to current line scalar
    my $prev  = shift;  # previous comma field scalar
    my $curr  = shift;  # current comma field scalar

    my $value = substr($$line, $prev, $curr-$prev);  # extract out the field from the line
    
    $value =~ s/""/"/g;  # remove escaped quotes
    
    push(@$field, $value);  # push field to an array, where each element is a field for that line

    return 1;
}


sub next {
    my $self = shift;
    my $line;
    if ($self->{'__fh'}->eof()) { 
	return;
    }
    while (defined( $line = $self->{'__fh'}->getline() )) {
	$line =~ s/[\r\n\s]+$//;
	if (length($line) > 0) {
	    last;
	} else {
	    return;
	}
    }

    my $fields = [];
    my $prev = 0;  # previous comma position in $line
    my $curr = 0;  # current char position in $line
    my $quoted = 0;
    my $open_quote = 0;
    my $prev_quote = -1;
    my $curr_quote = 0;
    my @curr_field = ();
    while ($curr < length($line)) {
	my $char = substr($line, $curr++, 1);

	if ($char =~ $CRLF_CHARACTER) {
	    next;

	} elsif ($char =~ $QUOTE_CHARACTER) {
	    my $curr_quote = $curr - 1;
	    if ($open_quote) {
		if (($prev_quote != -1) && ($curr_quote-$prev_quote == 0)) {
		    push(@curr_field,$char); # escaped quote character
		} else {
		    $prev_quote = $curr;
		}
		$open_quote = 0;  # close the quoted field
	    } else {
		$prev_quote = $curr;
		$open_quote = 1;  # open the quoted field
	    }
	} elsif ($char =~ $COMMA_CHARACTER) {
	    if ($open_quote) {  # the comma is escaped
		push(@curr_field, $char);
		
	    } else {
		push(@$fields,join('',@curr_field)//'');
		@curr_field = ();
		$prev = $curr; # set the last observed comma to current field
	    }
	} else {
	    push(@curr_field, $char);
	}
    }
    push(@$fields,join('',@curr_field)//'');

    if ($open_quote) {
	throw Exception("Malformed file");
    }
    
    return $fields;
}

1;

