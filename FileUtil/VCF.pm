# Copyright (c)2013. The Regents of the University of California (Regents).
# All Rights Reserved. Permission to use, copy, modify, and distribute this
# software and its documentation for educational, research, and
# not-for-profit purposes, without fee and without a signed licensing
# agreement, is hereby granted, provided that the above copyright notice,
# this paragraph and the following two paragraphs appear in all copies,
# modifications, and distributions. Contact The Office of Technology
# Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley, CA
# 94720-1620, (510) 643-7201, for commercial licensing opportunities.

# Created by Jessen Bredeson, Department of Molecular and Cell Biology, 
# University of California, Berkeley.

# IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
# SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

# REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
# HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
# MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.


package Valkyr::FileUtil::VCF;

use 5.010;
use strict;
use warnings;
use Exporter 'import';


use Valkyr::Error 'throw';
use Valkyr::Error::Exception {
    'RuntimeError' => [ 'MalformedVCF',
			'UnsupportedVCFVersion' ],
},  'ValueError', 'IOError';

use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_bool is_uint is_string is_arrayref is_hashref is_subclass);


use vars qw(@EXPORT @EXPORT_OK %EXPORT_TAGS);

BEGIN { 
    @EXPORT      
	= qw(CHROM POS ID REF ALT QUAL FILTER INFO FORMAT getEntry getSample
             getMeta addMeta setMeta indexArray getFILTER setFILTER getINFO
             setINFO getFORMAT setFORMAT);
    @EXPORT_OK
	= qw(NULL_ALLELE NULL_FIELD NULL_GT MAX_GQ EMPTY VCF_FIELD_SEP GT_FIELD_SEP 
             ALLELE_FIELD_SEP FORMAT_FIELD_SEP SAMPLE_FIELD_SEP GT_GROUP_PAT 
             GT_MATCH_PAT GT_PHASED GT_UNPHASED BREAKEND_PAT BREAKEND_MSG 
             PASS_FILTER_PAT);
    %EXPORT_TAGS 
	= ( 'standard'  => \@EXPORT,
	    'constants' => \@EXPORT_OK, 
	    'all'       => [@EXPORT,@EXPORT_OK] );
}


# constants for required VCF fields +optional FORMAT field
sub CHROM  () { return 0 }
sub POS    () { return 1 }
sub ID     () { return 2 }
sub REF    () { return 3 }
sub ALT    () { return 4 }
sub QUAL   () { return 5 }
sub FILTER () { return 6 }
sub INFO   () { return 7 }
sub FORMAT () { return 8 }


# convenience constants
sub HEADER_START     () { return '#' }
sub META_START       () { return '##' }
sub GT_PHASED        () { return '|' }
sub GT_UNPHASED      () { return '/' }
sub VCF_FIELD_SEP    () { return "\t" }
sub INFO_KVPAIR_SEP  () { return '=' }
sub INFO_FIELD_SEP   () { return ';' }
sub ALLELE_FIELD_SEP () { return ',' }
sub FORMAT_FIELD_SEP () { return ':' }
sub FILTER_FIELD_SEP () { return ';' }
sub SAMPLE_FIELD_SEP () { return ':' }
sub BREAKEND_MSG     () { return 'Breakends not supported, skipping (%s:%u).' }
sub NULL_ALLELE      () { return '.' }
sub NULL_FIELD       () { return '.' }
sub MAX_GQ           () { return  99 }
sub EMPTY            () { return  '' }

# accelerate pattern matching by precompiling
my $META_LINE_START_PAT    = qr/^##/;
my $META_LINE_FIELD_PAT    = qr/^##([^=]+)=/;
my $META_FIRSTLINE_PAT     = qr/^##fileformat=VCFv(\d+\.\d+)/;
my $META_ARRAY_PAT         = qr/^<|>$/;
my $META_ARRAY_KVPAIR_PAT  = qr/^([^=]+)=([^"=,]+|".*"),?/;
my $HEADER_FIRSTLINE_PAT   = qr/^#CHROM/;
my $HEADER_LINE_START_PAT  = qr/^#/;
my $GT_FIELD_SEP_PAT       = qr/[\/|]/;
my $GT_MATCH_PAT           = qr/^(?:\d+|\.)[\/|](?:\d+|\.)$/;
my $GT_GROUP_PAT           = qr/^(\d+|\.)[\/|](\d+|\.)$/;
my $BREAKEND_PAT           = qr/[\[\]\(\)]/;
my $NON_WHITESPACE_PAT     = qr/\S+/;
my $PASS_FILTER_PAT        = qr/^(?:PASS|\.)$/;
my $NULL_LINE_PAT          = qr/^#|^\s*$/;
my $NULL_GT_STR            = join(GT_UNPHASED,NULL_ALLELE,NULL_ALLELE);


# but also make the patterns exportable:
sub NULL_GT          () { return $NULL_GT_STR }
sub GT_FIELD_SEP     () { return $GT_FIELD_SEP_PAT }
sub GT_MATCH_PAT     () { return $GT_MATCH_PAT }
sub GT_GROUP_PAT     () { return $GT_GROUP_PAT }
sub BREAKEND_PAT     () { return $BREAKEND_PAT }
sub NULL_LINE_PAT    () { return $NULL_LINE_PAT }
sub PASS_FILTER_PAT  () { return $PASS_FILTER_PAT }


sub getEntry {
    my $fh = is_subclass(shift,'IO::Handle');
    my $N  = is_uint($_[0] ? shift : 10);

    while (defined( my $line = $fh->getline() )) {
	$line =~ $NULL_LINE_PAT && next;
	chomp($line);
	
	my @vcf = split(VCF_FIELD_SEP,$line);

	# do some basic entry checking for required fields
	if (@vcf < FORMAT) {
	    throw MalformedVCF("Missing VCFv4.0 fixed-field(s).");

	} elsif (@vcf > FORMAT) {
	    throw MalformedVCF(sprintf('No samples detected (%s:%u).',
		      @vcf[0,1])) if @vcf < FORMAT+2;
	    throw MalformedVCF(sprintf('Unexpected number of fields: '.
		      '%u != %u (%s:%u).',scalar(@vcf),$N,@vcf[0,1])) 
                      if @vcf != $N;
	}
	return(\@vcf);
    }
}


sub getSample {
    my $smpl = is_string(shift);
    my $fmt  = is_hashref(shift);

    my @smpl = split(SAMPLE_FIELD_SEP,$smpl);
    if (! defined($fmt->{'GT'})) {
	throw MalformedVCF('FORMAT-GT field required.');
    }
    if ($smpl[ $fmt->{'GT'} ] eq NULL_ALLELE) {
	$smpl[ $fmt->{'GT'} ] = NULL_GT;
    }
    if ($smpl[ $fmt->{'GT'} ] !~ $GT_MATCH_PAT) {
	throw MalformedVCF(sprintf('Malformed Sample-GT (%s).',
		      $smpl[ $fmt->{'GT'} ]||'undef'));
    }

    for my $field (keys(%$fmt)) {
	if (! defined($smpl[ $fmt->{$field} ])) {
	    $smpl[ $fmt->{$field} ] //= NULL_FIELD;
	}
    }

    return @smpl;
}


sub addMeta {
    my $META = is_hashref(shift);
    my $tag  = is_string(shift);
    my $meta = shift;

    if (Valkyr::Data::Type::Test::is_hashref($meta)) {
	if (! defined($meta->{'ID'})) {
	    throw MalformedVCF("Malformed '$tag' compound Meta line: No 'ID' sub-field.");

	} elsif (! defined($meta->{'_ORDER_'})) {
	    throw MalformedVCF('No Meta sub-field order specified.');

	} else {
	    $META->{$tag}->{'_ORDER_'} ||= [];
	    $META->{$tag}->{ $meta->{'ID'} } = $meta;
	    push(@{$META->{$tag}->{'_ORDER_'}},$meta->{'ID'});
	    return 1;
	}

    } elsif (Valkyr::Data::Type::Test::is_string($meta)) {
	$META->{$tag} = $meta;
	return 1;

    } else {
	throw TypeError('Unsupported Meta input data type: ',ref($meta)||'undef','.');
    }
}


sub parse_tag {
    my $string = is_string(shift);
    
    if ($string =~ s/$META_ARRAY_PAT//g) { # array
	
	my %meta = ('_ORDER_' => []);
	while ($string =~ s/$META_ARRAY_KVPAIR_PAT//) {
	    $meta{$1} = $2;
	    push @{$meta{'_ORDER_'}},$1;
	}
	return(\%meta);

    } else {
	return $string;
    }
}    


sub getMeta {
    my $fh   = is_subclass(shift,'IO::Handle');
    my $skip = is_bool(shift||0);

    my %META;
    my $firstline = EMPTY;
    while ($firstline = <$fh>) {
	if($firstline =~ $NON_WHITESPACE_PAT) { last }
    }

    if (! defined($firstline) || 
	$firstline !~ $META_FIRSTLINE_PAT) {
	throw MalformedVCF('Missing required VCF \'fileformat\' Meta line.');

    } elsif ($1 < 4) {
	throw UnsupportedVCFVersionError("VCFv4.0+ supported only ($1 < 4.0).");

    } else {
	$META{'fileformat'} = "VCFv$1";
    }

    while (local $_ = <$fh>) {
	if (m/$META_LINE_START_PAT/ && $skip) { next }
	if (s/$META_LINE_FIELD_PAT//) {
	    chomp();
	    my ($tag,$val) = ($1,parse_tag($_));
	    if (! defined($val)) {
		throw MalformedVCF("Malformed '$tag' Meta line.");
	    }
	    addMeta(\%META,$tag,$val);

	} elsif (m/$HEADER_FIRSTLINE_PAT/) {
	    s/$HEADER_LINE_START_PAT//; 
	    chomp();

	    my @HDR = split(VCF_FIELD_SEP);
	    my @req = qw(CHROM POS ID REF ALT QUAL FILTER INFO);
	    for (my $i = 0; $i < FORMAT; ++$i) {
		if ($HDR[$i] ne $req[$i]) {
		    throw MalformedVCF("Malformed VCF Header line: [@HDR]");
		}
	    }

	    if (@HDR > 8) { 
		if ((! defined($HDR[8])) || ($HDR[8] ne 'FORMAT')) {
		    throw MalformedVCF('Samples detected without \'FORMAT\' Header field.');
		} elsif (@HDR < 10) {
		    throw MalformedVCF('\'FILTER\' Header field detected without samples.');
		}
	    }
	    return( \%META, \@HDR );
	    
	} else {
	    throw MalformedVCF('Missing/malformed VCF header.');
	}
    }
}

sub setMeta {
    my $META = is_hashref(shift);
    my $HDR  = is_arrayref(shift);
    
    my @meta;
    if (! Valkyr::Data::Type::Test::is_string($META->{'fileformat'})) {
	throw MalformedVCF('Missing required VCF \'fileformat\' Meta line.');

    } else {
	push(@meta,META_START.'fileformat='.$META->{'fileformat'});
    }
    
    for my $field (sort(keys(%$META))) {
	if (! defined($META->{$field})) { next }
	if ($field eq 'fileformat') { next }

	if (Valkyr::Data::Type::Test::is_hashref($META->{$field})) {
	    if (! Valkyr::Data::Type::Test::is_arrayref($META->{$field}->{'_ORDER_'})) {
		throw MalformedVCF('Incomplete Meta information for write.')
	    }

	    for my $tag ( @{$META->{$field}->{'_ORDER_'}} ) {
		my $val = $META->{$field}->{is_string($tag)};
		if (! Valkyr::Data::Type::Test::is_arrayref($val->{'_ORDER_'})) {
		    throw MalformedVCF('Incomplete Meta information for write.');
		}

		my @subfields;
		for my $subfield (@{ $val->{'_ORDER_'} }) {
		    if (! defined($val->{is_string($subfield)})) { next }
		    push(@subfields, $subfield.'='.is_string($val->{$subfield}));
		}
		push(@meta,sprintf('%s%s=<%s>',META_START,$field,join(',',@subfields)));
	    }

	} else {
	    push(@meta,sprintf('%s%s=%s',META_START,$field,is_string($META->{$field})));
	}
    }

    if (@$HDR < 8) {
	throw MalformedVCF("Malformed VCF Header line: [@$HDR]");
    }

    push(@meta,HEADER_START.join(VCF_FIELD_SEP,map {is_string($_)} @$HDR));
    
    return join("\n",@meta);
}		    


sub indexArray {
    my $i = 0;
    my %f = map {$_ => $i++} @_;

    return wantarray ? (\%f,$i) : \%f;
}


sub getFILTER {
    my $filter = is_string(shift);
    
    if ($filter eq NULL_FIELD() || $filter =~ $PASS_FILTER_PAT) { return {} }

    return scalar(indexArray(split(FILTER_FIELD_SEP,$filter)));
}


sub setFILTER {
    my $filter = is_hashref(shift);
    
    if (! %$filter ) { return 'PASS' }

    return join(FILTER_FIELD_SEP,sort keys(%$filter) );
}


sub getINFO {
    my $info = is_string(shift);
    
    if ($info eq NULL_FIELD) { return {} }
    
    my %info;
    for my $tag (split(INFO_FIELD_SEP,$info)) {
	my ($k,$v) = split(INFO_KVPAIR_SEP,$tag);
	$info{$k} = $v;
    }
    return \%info;
}


sub setINFO {
    my $info = is_hashref(shift);
    
    if (! %$info ) { return NULL_FIELD }

    my @tags;
    for my $tag (sort keys %$info) {
	if (!defined( $info->{$tag} )) {
	    push(@tags,$tag);
	} else {
	    push(@tags,join(INFO_KVPAIR_SEP,$tag,$info->{$tag}));
	}
    }
    return join(INFO_FIELD_SEP,@tags);
}


sub getFORMAT {
    my $fmt = is_string(shift);
    my $opt = is_arrayref(shift||[]);

    my ($f,$i) = indexArray(split(FORMAT_FIELD_SEP,$fmt));

    if (! defined($f->{'GT'})) {
	throw MalformedVCF('FORMAT-GT field required.');
    }
    if ($f->{'GT'} != 0) {
	throw MalformedVCF('FORMAT-GT not first field.');
    }
    for my $field (@$opt) {
	if (! defined($f->{$field})) {
	    $f->{$field} = $i++;
	}
    }

    return wantarray ? ($f,$i) : $f;
}


sub setFORMAT {
    my $format = is_hashref(shift);
    
    my %rformat = reverse(%$format);
    
    return join(FORMAT_FIELD_SEP,map {$rformat{$_}} sort {$a <=> $b} keys %rformat);
}


sub is_indel {
    my $ref = is_string(shift);
    my $alt = is_string(shift);

    my $ind = 0;
    if (length($ref) != 1 || length($alt) != length($ref)) {
	my $maxlen = 0;
	my @allele = ($ref,split(ALLELE_FIELD_SEP,$alt));
	for(my $i  = 0; $i < @allele; ++$i) {
	    if (length($allele[$i]) > $maxlen) {
		$maxlen = length($allele[$i]);
	    }
	}
	for(my $i  = 0; $i < @allele; ++$i) {
	    if (length($allele[$i]) < $maxlen) { 
		$ind |= (1 << $i);
	    }
	}
    }
    return $ind;
}


1;
