package Valkyr::FileUtil::Fasta;

use 5.010;
use strict;
use warnings;
use Exporter 'import';

use Valkyr::Error qw(report warn throw);
use Valkyr::Error::Exception {
    'RuntimeError' => ['MalformedFaidx',
	               'MalformedFasta'],
    'IOError'      => ['SeekError'],
},  'IOError', 'ValueError', 'KeyError';


use Valkyr::FileUtil::IO;
use Valkyr::Class::Constants 'EMPTY';
use Valkyr::Data::Type::Test;
use Valkyr::Data::Type::Assert 
    qw(is_uint is_file is_string is_hashref is_arrayref is_subclass);


use vars qw($PREVSEEK @EXPORT_OK $FAHDR_CHAR $FAHDR_PAT);

BEGIN {
    $FAHDR_CHAR = '>';
    $FAHDR_PAT  = qr/^$FAHDR_CHAR[\011\040]*/;
    $PREVSEEK   = -1;
    @EXPORT_OK  = qw(nextEntry pullEntry printEntry faidx readFaidx faidxName);
}

sub NAME   () { return 0 }
sub LENGTH () { return 1 }
sub OFFSET () { return 2 }
sub LINE_LEN   () { return 3 }
sub LINE_BLEN  () { return 4 }
sub NULL_LINE  () { return qr/^#|^\s*$/ }
sub FAIDX_LINE () { return qr/([!-)+-;=?-~][!-~]*)\t(\d+)\t(\d+)\t\d+/ }
sub FAHDR_LINE () { return qr/^$FAHDR_CHAR[\011\040]*([!-)+-;=?-~][!-~]*)(.*?)[\012\014\015]?$/ }
sub FASEQ_LINE () { return qr/^[a-zA-Z\-]+[\012\014\015]?$/ }

sub parseFastaBlock {
    local $_ = is_string(shift);

    s/$FAHDR_CHAR$//;
    s/$FAHDR_PAT//;
    
    my $fa = [];
    if (/^([!-)+-;=?-~][!-~]*)(.*?)[\012\014\015]([a-zA-Z\-\012\014\015]+)/s) {
	$fa->[0] = $1;
	$fa->[1] = $2;
	$fa->[2] = $3;
	$fa->[2] =~ s/\s+//g;

	return $fa;

    } else {
	throw MalformedFasta('Incomplete sequence entry detected.');
    }
}


sub nextEntry {
    my $fh = is_subclass(shift,'IO::Handle');
    
    if ($fh->eof()) { return }

    local $/ = "\n$FAHDR_CHAR";

    return parseFastaBlock( $fh->getline() );
}


sub printEntry {
    my $fh = is_subclass(shift,'IO::Handle');
    my $fa = is_arrayref(shift);

    print($fh join("\n",$FAHDR_CHAR.
		   is_string($fa->[0]).is_string($fa->[1]),
		   is_string($fa->[2]) =~ /(.{1,60})/g,''
	  ));

    return 1;
}


sub pullEntry {
    my $fh  = is_subclass(is_subclass(shift,'IO::Handle'),'IO::Seekable');
    my $fai = is_hashref(shift);
    my $id  = is_string(shift);
    my $idx = $fai->{$id};


    if (Valkyr::Data::Type::Test::is_hashref($idx)) {
	if (!defined($idx->{'offset'})) {
	    throw KeyError("Faidx entry offset unavailable: $id.");
	}
	if (!defined($idx->{'length'})) {
	    throw KeyError("Faidx entry length unavailable: $id.");
	}
    } else {
	throw KeyError("Faidx entry ID unavailable.");
    }

    if ($idx->{'offset'} < $PREVSEEK && 
	Valkyr::Data::Type::Test::is_subclass($fh,'IO::Compress::Base') ||
	Valkyr::Data::Type::Test::is_subclass($fh,'IO::Uncompress::Base')) {
	throw SeekError('IO::Compress::* and IO::Uncompress::* ',
			'derived objects cannot seek backwards.')
    }
    if (! $fh->seek(is_uint($idx->{'offset'}),0)) {
	throw SeekError("Failed to seek() to fasta position: ",
			$idx->{'offset'});
    }

    my $seq = EMPTY;
    while  (defined( my $line = $fh->getline() )) {
	if ($line =~ $FAHDR_PAT) { last }
	$seq .= $line;
    }
    $seq =~ s/\s//g;

    if (length($seq) != $idx->{'length'}) {
	throw ValueError(sprintf("Incomplete sequence entry detected (%u != %u): $id",
			 length($seq)||0,$idx->{'length'}||0));
    }

    $PREVSEEK = $idx->{'offset'};

    return [ $id, EMPTY, $seq ];
}
    

sub faidxName {
    my $infile = is_file(shift,'f');
    my $prefix = $infile;

    if ($infile =~ /\.fai$/) {
	return $infile;
    }

    $prefix =~ s/\.f(ast|n)?a$//;
    if (Valkyr::Data::Type::Test::is_file("$prefix.fai",'f')) {
	$infile = "$prefix.fai";
    } else {
	$infile = "$infile.fai";
    }
    return $infile;
}


sub faidx {
    my $infile = is_string(shift);

    my $fdi = open($infile,READ,NOSTREAM);
    my $fdo = open(faidxName($infile),WRITE,NOSTREAM);

    my $line = EMPTY; 
    my @INIT = (undef,0,0,0,0); 
    my ($off,@FAI) = (0,@INIT);

    # scan forward for first fasta entry line:
    do { $line = $fdi->getline(); $off += length($line) } while $line !~ /\S+/;

    if ($line !~ FAHDR_LINE) {
	throw(sprintf('File not in valid fasta format (%s, line %u). ',
		      $infile,$fdi->input_line_number())); 
    }

    @FAI[NAME,OFFSET] = ($1,$off);
    
    while ($line = $fdi->getline()) {
	if($line =~ $FAHDR_PAT) { 
	    throw(sprintf("No sequence string found at (line %u).",
			  $fdi->input_line_number()));
	}
	my $seq = $line;
	my $len = length($line);
	$FAI[LINE_BLEN] = $len;
	$FAI[LINE_LEN]  = $len - 1;
	$off += $FAI[LINE_BLEN];

	my $i = 0;
	while ($line = $fdi->getline()) {
	    $off += length($line);
	    $line =~ FAHDR_LINE && last;
	    $seq .= $line;
	    $len  = length($line);
	    $i++;
	}
	# now make sure our sequence is flush (ignoring last line)
	if ((length($seq) - $len) != ($i*$FAI[LINE_BLEN])) {
	    throw('Different line length in sequence: ',($FAI[NAME]||''));
	}
	$seq =~ s/\s+//g;
	
	$FAI[LENGTH] = length($seq);
	
	print($fdo join("\t",@FAI),"\n");
	
	if ((!defined($line) || $line !~ FAHDR_LINE) && !$fdi->eof()) {
	    throw(sprintf('No seqeunce ID detected (%s, line %u)',
			  $infile,$fdi->input_line_number()));
	}
	@FAI = @INIT; # reset
	@FAI[NAME,OFFSET] = ($1,$off);
    }
    close($fdo);
    close($fdi);

    return 1;
}

sub readFaidxHashed {
    my $fafile = is_file(shift,'f');
    my $infile = faidxName($fafile);
    
    # try to detect all valid .fai file names

    my $i = 0;
    my $idx = {};
    my $fdi = open($infile,READ,NOSTREAM);
    while  (defined( my $line = $fdi->getline() )) {
        if ($line =~ NULL_LINE) { next }
        if ($line !~ FAIDX_LINE) {
            throw MalformedFaidx(sprintf('Invalid or insufficient '.
		  'data (%s, line %s).',$infile,$fdi->input_line_number()));
	}
        if (exists($idx->{$1})) {
            warn(sprintf('Duplicate faidx entry detected '.
		  '(%s, line %s).',$infile,$fdi->input_line_number()));
	}
	$idx->{$1}->{'contig'} = $1;
        $idx->{$1}->{'length'} = $2;
	$idx->{$1}->{'offset'} = $3;
	$idx->{$1}->{'index'}  = $i++;
	$idx->{$1}->{'fasta'}  = $fafile;
    }
    $fdi->close();

    return $idx;
}


sub readFaidxOrdered {
    my $fafile = is_file(shift,'f');
    my $infile = faidxName($fafile);
    
    # try to detect all valid .fai file names

    my $i = 0;
    my @idx;
    my %seen;
    my $fdi = open($infile,READ,NOSTREAM);
    while  (defined( my $line = $fdi->getline() )) {
        if ($line =~ NULL_LINE) { next }
        if ($line !~ FAIDX_LINE) {
            throw MalformedFaidx(sprintf('Invalid or insufficient '.
		  'data (%s, line %s).',$infile,$fdi->input_line_number()));
	}
        if ($seen{$1}) {
            warn(sprintf('Duplicate faidx entry detected '.
		  '(%s, line %s).',$infile,$fdi->input_line_number()));
	}
	push(@idx,
	     {
		 'contig' => $1,
		 'length' => $2,
		 'offset' => $3,
		 'index'  => $i++,
		 'fasta'  => $fafile,
	     });
    }
    $fdi->close();

    return \@idx;
}


sub readFaidx {
    my $fna = is_file(shift,'f');
    my $fai = faidxName($fna);
    my $opt = is_uint(shift//0);      
    
    if (!Valkyr::Data::Type::Test::is_file($fai,'f') || 
       (is_older($fai,$fna) < 0)) {
	warn("FASTA index out of date, re-indexing...");
	faidx($fna);
    }
    if ($opt) { 
	return readFaidxOrdered($fna);
    } else {
	return readFaidxHashed($fna);
    }
}



1;
