# Valkyr::DRM::Scheduler - Generic DRM scheduler handler methods

package Valkyr::DRM::Scheduler;

use strict;
use warnings;

use vars qw($DRM_SCHEDULER);
use base qw(Valkyr::DRM::Scheduler::AbstractBase);

use File::Util;
use Valkyr::Error qw(throw debug);
use Valkyr::Error::Exception qw(NameError RequireError 
                                UnmatchedArgError RequiredArgError);
use Valkyr::Data::Type::Assert qw(is_string is_subclass);


BEGIN { $DRM_SCHEDULER = __PACKAGE__ }

sub __initialize {
    my ($self,@args) = @_;

    return if ! @args;

    throw UnmatchedArgError('Unmatched args detected.') if @args % 2;
    while (my $key = shift @args) {
	my $method = lc($key);
	my $value  = shift @args;
	
	$method =~ s/^-{0,2}//;
	if ($self->can($method)) {
	    $self->$method($value);
	} else {
	    throw NameError("Unrecognized argument: $method");
	}
    }
    return 1;
}

sub __load_scheduler {
    my ($pkg,$class) = @_;
    
    local $@ = undef;
    eval "require $class";
    
    debug("Attempting to load $class: ",$@||'SUCCESS');
    throw RequireError("Please verify install of module, could ",
		       "not load: $class") if $@;
    
    return $class->new();
}

sub new { # inspired by Bio::SeqIO
    my ($pkg,@args) = @_;
    
    my  $self;
    my  $class = ref($pkg) || $pkg;
    if ($class =~ /^$DRM_SCHEDULER\::\w+/) {
	$self  = $class->SUPER::new(@args);
	$self->__initialize(@args);
	return $self;
    } else {
	throw UnmatchedArgError('Unmatched args detected.') if @args % 2;

	my $j = @args;
	for (my $i = 0; $i < @args; $i += 2) {
	    $args[$i] =~ s/^-{0,2}//;
	    $args[$i] =  lc($args[$i]);
	    if ($args[$i] eq 'scheduler') { $j = $i }
	}
	if ( ! defined($args[$j])) {
	    throw RequiredArgError('Param "scheduler" not specified.');
	} else {
	    $class = is_string($args[$j+1]);
	    $class =~ s/^(?:$DRM_SCHEDULER\::)?/$DRM_SCHEDULER\::/;
	    $self  = __PACKAGE__->__load_scheduler($class);
	    $self->__initialize(@args);
	    return $self;
	}
    }
}

sub __format_task_string {
    my $self = shift;
    my $job  = is_subclass(shift,'Valkyr::DRM::Job');

    my @range;
    my @tasks = sort {$a <=> $b} $job->get_task_ids();
    @tasks > 0 || throw ValueError('No tasks defined.');

    # collapse consecutive numbers into ranges of numbers;
    # the output string will have the form: 1-5,7,10
    my $start = $tasks[0]; 	    # initialize start.
    unshift(@tasks,$tasks[0]);	    # pad the beginning.
    push(@tasks,$tasks[$#tasks]+2); # pad the end.
    for(my $i = 1; $i < @tasks; ++$i) {
	if (($tasks[$i] - $tasks[$i-1] > 1) || ($i == $#tasks)) {
	    push(@range, ($tasks[$i-1] - $start)
		 ? $start.'-'.$tasks[$i-1] : $tasks[$i-1]);
	    $start = $tasks[$i];
	}
    }
    return join(',',@range);
}

sub __mark_tasks_complete {
    my $self = shift;
    my $job  = is_subclass(shift,'Valkry::Grid::Job');

    my $nComplete = 0;
    for my $task (@_) {
	if ($job->exists_task_id($task)) {
	    $nComplete += $job->update_field($task,1);
	    # should also delete the .lock file if it exists
	}
    }
    return $nComplete;
}


1;
