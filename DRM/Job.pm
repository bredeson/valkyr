package Valkyr::DRM::Job;

use strict;
use warnings;

use base 'Valkyr::OrderedHash';

use Valkyr::Error 'throw';
use Valkyr::Error::Exception 'ValueError';
use Valkyr::Data::Type::Assert qw(is_uint is_string is_file);

# An abstract Job object shall be a container object;
# It shall contain:
# 1) job (qstat) status information:
# 2) task touch-file paths
# 3) 

# for backward compatibility:
sub __validate_field {
    my ($self,$key) = @_;
    is_uint($key);
    return 1;
}

*get_task_ids = \&Valkyr::OrderedHash::get_field_keys;

*delete_task_id = \&Valkyr::OrderedHash::delete_field;

*exists_task_id = \&Valkyr::OrderedHash::exists_field;

*get_task_completion_status = \&Valkyr::OrderedHash::get_field;

sub set_job_id {
    my $self = shift;
    $self->{'_jid'} = is_uint(shift) if @_;
    return defined($self->{'_jid'});
}

sub get_job_id { return shift->{'_jid'} }

sub set_task_ids {
    my ($self,@keys) = @_;
    
    for my $key (@keys) {
	is_uint($key); # assert
	$self->delete_task_id($key) if $self->exists_task_id($key);
	$self->__field()->{'_'.$key} = 0;
	push @{$self->__index()},$key;
    }
    return scalar(@{$self->__index()});
}    

sub set_owner {
    my $self = shift;
    $self->{'_owner'} = is_string(shift) if @_;
    return defined($self->{'_owner'});
}

sub get_owner { return shift->{'_owner'} }

sub set_uid {
    my $self = shift;
    $self->{'_uid'} = is_uint(shift) if @_;
    return defined($self->{'_uid'});
}

sub get_uid { return shift->{'_uid'} }

sub set_group {
    my $self = shift;
    $self->{'_group'} = is_string(shift) if @_;
    return 1;
}

sub get_group { return shift->{'_group'} }

sub set_gid {
    my $self = shift;
    $self->{'_gid'} = is_uint(shift) if @_;
    return defined($self->{'_gid'});
}

sub get_gid { return shift->{'_gid'} }

sub set_client_home {
    my $self = shift;
    $self->{'_client_home'} = is_string(shift) if @_;
    return defined($self->{'_client_home'});
}   

sub get_client_home { return shift->{'_client_home'} }

sub set_client_host {
    my $self = shift;
    $self->{'_client_host'} = is_string(shift) if @_;
    return defined($self->{'_client_host'});
}   

sub get_client_host { return shift->{'_client_host'} }

sub set_client_logname {
    my $self = shift;
    $self->{'_client_logname'} = is_string(shift) if @_;
    return defined($self->{'_client_logname'});
}   

sub get_client_logname { return shift->{'_client_logname'} }

sub set_client_mail {
    my $self = shift;
    $self->{'_client_mail'} = is_file(shift,'dwx') if @_;
    return defined($self->{'_client_mail'});
}

sub get_client_mail { return shift->{'_client_mail'} }

sub set_client_path {
    my $self = shift;
    $self->{'_client_path'} = is_string(shift) if @_;
    return defined($self->{'_client_path'});
}   

sub get_client_path { return shift->{'_client_path'} }

sub set_client_shell {
    my $self = shift;
    $self->{'_client_shell'} = is_file(shift,'fx') if @_;
    return defined($self->{'_client_shell'});
}   

sub get_client_shell { return shift->{'_client_shell'} }

sub set_client_workdir {
    my $self = shift;
    $self->{'_client_workdir'} = is_file(shift,'dwx') if @_;
    return defined($self->{'_client_workdir'});
}

sub get_client_workdir { return shift->{'_client_workdir'} }

sub set_account {
    my $self = shift;
    $self->{'_account'} = is_string(shift) if @_;
    return defined($self->{'_account'});
}

sub get_account { return shift->{'_account'} }

sub set_job_workdir {
    my $self = shift;
    $self->{'_job_workdir'} = is_file(shift,'dwx') if @_;
    return defined($self->{'_job_workdir'});
};

sub get_job_workdir { return shift->{'_job_workdir'} }

sub set_hard_resource {
    my $self = shift;
    $self->{'_hard_resource'} = is_string(shift) if @_;
    return defined($self->{'_hard_resource'});
}   

sub get_hard_resource { return shift->{'_hard_resource'} }

sub notify_set {
    my $self = shift;
    $self->{'_notify'} = is_bool(shift) if @_;
    return $self->{'_notify'};
}

sub set_job_name { 
    my $self = shift;
    $self->{'_job_name'} = is_string(shift) if @_;
    return defined($self->{'_job_name'});
}

sub get_job_name { return shift->{'_job_name'} }

sub set_job_share {
    my $self = shift;
    $self->{'_job_share'} = is_float(shift) if @_; # ???
    return defined($self->{'_job_share'});
}

sub get_job_share { return shift->{'_job_share'} }

sub set_hard_queue {
    my $self = shift;
    $self->{'_hard_queue'} = is_string(shift) if @_;
    return defined($self->{'_hard_queue'});
}

sub get_hard_queue { return shift->{'_hard_queue'} }

sub set_project_name {
    my $self = shift;
    $self->{'_project_name'} = is_string(shift) if @_;
    return defined($self->{'_project_name'});
}

sub get_project_name { return shift->{'_project_name'} }

sub set_script_name {
    my $self = shift;
    $self->{'_script_name'} = is_file(shift,'fr') if @_;
    return defined($self->{'_script_name'});
}

sub get_script_name { return shift->{'_script_name'}} 

sub set_batch_name {
    my $self = shift;
    $self->{'_batch_name'} = is_file(shift,'fr') if @_;
    return defined($self->{'_batch_name'});
}

sub get_batch_name { return shift->{'_batch_name'} }



1;
